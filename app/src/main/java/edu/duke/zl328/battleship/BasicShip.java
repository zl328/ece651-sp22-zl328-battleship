package edu.duke.zl328.battleship;

import java.util.ArrayList;
import java.util.HashMap;

public abstract class BasicShip<T> implements Ship<T> {
    /**
     * The ship index contains the ship coordinate in a specific sequence
     */
    protected ArrayList<Coordinate> shipIndex;
    /**
     * contains the coordinates and if hit or not
     */
    protected HashMap<Coordinate, Boolean> myPieces;
    /**
     * is the display info of myself
     */
    protected ShipDisplayInfo<T> myDisplayInfo;
    /**
     * the display info of enemy
     */
    protected ShipDisplayInfo<T> enemyDisplayInfo;

    /**
     * Constructor of basicship
     * 
     * @param where            a hashset of coordinates. Initialize mypieces to
     *                         false as they are not hit yet
     * @param indexShip        the specific sequence of coordinates
     * @param myDisplayInfo    my own ship display information.
     * @param enemyDisplayInfo the display information of the enemy.
     */
    public BasicShip(Iterable<Coordinate> where, Iterable<Coordinate> indexShip, ShipDisplayInfo<T> myDisplayInfo,
            ShipDisplayInfo<T> enemyDisplayInfo) {
        shipIndex = new ArrayList<Coordinate>();
        myPieces = new HashMap<Coordinate, Boolean>();
        for (Coordinate c : indexShip) {
            shipIndex.add(c);
        }
        for (Coordinate c : where) {
            myPieces.put(c, false);
        }
        this.myDisplayInfo = myDisplayInfo;
        this.enemyDisplayInfo = enemyDisplayInfo;
    }

    /**
     * Display the info of ships if they are hit or missed.
     * 
     * @param where  is the coordinate being fired at.
     * @param myShip is to determine if the ship is my or the enemy's ship.
     * @return the whatever display info
     */
    @Override
    public T getDisplayInfoAt(Coordinate where, boolean myShip) {
        checkCoordinateInThisShip(where);
        if (myShip == true) {
            return myDisplayInfo.getInfo(where, myPieces.get(where));

        } else {
            return enemyDisplayInfo.getInfo(where, myPieces.get(where));
        }
    }

    /**
     * Used to check if a ship occupies the coordinate.
     * 
     * @param where is the coordinate to check.
     * @return if the coordinate is occupied.
     */
    public boolean occupiesCoordinates(Coordinate where) {
        if (myPieces.get(where) == null) {
            return false;
        }
        return true;
    }

    /**
     * Check if this basic ship occupies the given coordinate.
     * 
     * @param where is the Coordinate iterable to check if this Ship occupies
     * @return true if where is inside this ship, false if not.
     */
    public boolean occupiesCoordinates(Iterable<Coordinate> where) {
        for (Coordinate c : where) {
            if (myPieces.get(c) == null) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check if the coordinate is on ship.
     * 
     * @param c the coordinate to be checked.
     * @throws IllegalArgumentException if c is not on board.
     */
    protected void checkCoordinateInThisShip(Coordinate c) {
        if (myPieces.get(c) == null) {
            throw new IllegalArgumentException("The coordinate is not on ship!\n");
        }
    }

    /**
     * Check if the ship is sunk(All coordinates of this
     * ship are compromised).
     * 
     * @return a boolean if the ship is sunk.
     */
    @Override
    public boolean isSunk() {
        for (boolean ans : myPieces.values()) {
            if (ans == false) {
                return false;
            }
        }
        return true;
    }

    /**
     * Record a fire towards the coordinate.
     */
    @Override
    public void recordHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        myPieces.put(where, true);
    }

    /**
     * Check if the coordinate has been hit before
     */
    @Override
    public boolean wasHitAt(Coordinate where) {
        checkCoordinateInThisShip(where);
        return myPieces.get(where);
    }

    /**
     * Get all the coordinate of the single ship.
     */
    @Override
    public Iterable<Coordinate> getCoordinates() {
        return myPieces.keySet();
    }

    /**
     * getter function for shipIndex
     */
    public ArrayList<Coordinate> getShipIndex() {
        return this.shipIndex;
    }

    /**
     * getter function for myPieces
     */
    public HashMap<Coordinate, Boolean> getMyPieces() {
        return this.myPieces;
    }

    /**
     * return myDisplayInfo
     */
    public ShipDisplayInfo<T> getMyDisplayInfo() {
        return this.myDisplayInfo;
    }

    /**
     * return enemyInfo
     */
    public ShipDisplayInfo<T> getEnemyDisplayInfo() {
        return this.enemyDisplayInfo;
    }

    /**
     * copy the "hit" info of the ship. Called from make movement class
     */
    @Override
    public void copyHitInfo(Ship<T> shipToCopy) {
        for (Coordinate coordinate : shipToCopy.getShipIndex()) {
            if (shipToCopy.getMyPieces().get(coordinate) == true) {
                this.myPieces.put(this.shipIndex.get(shipToCopy.getShipIndex().indexOf(coordinate)), true);
            }
        }
        return;
    }
}
