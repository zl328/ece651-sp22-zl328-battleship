package edu.duke.zl328.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.MissingFormatWidthException;

import javax.print.attribute.HashAttributeSet;

import org.checkerframework.checker.units.qual.C;

/**
 * BattleSHipBoard
 */
public class BattleShipBoard<T> implements Board<T> {
    private final int width;

    private final int height;

    final ArrayList<Ship<T>> myShips;

    private final PlacementRuleChecker<T> placementChecker;

    HashSet<Coordinate> enemyMisses;

    final T missInfo;

    protected HashMap<Coordinate, Character> hitHistory;

    /**
     * Constructs a BattleShipBoard with the specified width
     * and height.
     * 
     * @param w        is the width of the constructed board.
     * @param h        is the height of the constructed board.
     * @param missInfo is the character when a player miss fired
     * @throws IllegalArgumentException if the width or heitght are less than or
     *                                  equal to zero.
     */
    public BattleShipBoard(int w, int h, T missInfo) {
        if (w <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's width must be positive but is " + w);
        }
        if (h <= 0) {
            throw new IllegalArgumentException("BattleShipBoard's height must be positive but is " + h);
        }
        this.height = h;
        this.width = w;
        this.myShips = new ArrayList<Ship<T>>();
        this.placementChecker = new InBoundsRuleChecker<T>(new NoCollisionRuleChecker<T>(null));
        this.enemyMisses = new HashSet<Coordinate>();
        this.missInfo = missInfo;
        this.hitHistory = new HashMap<Coordinate, Character>();
    }

    /**
     * a getter method for height of the board
     */
    public int getHeight() {
        return height;
    }

    /**
     * a getter method for width of the board
     */
    public int getWidth() {
        return width;
    }

    /**
     * Add ship to myShips array list method
     * 
     * @param toAdd is the ship to be added to the array list
     * @return the validity of the placement
     */
    public String tryAddShip(Ship<T> toAdd) {
        String message = placementChecker.checkPlacement(toAdd, this);
        if (message == null) {
            myShips.add(toAdd);
        }
        return message;
    }

    /**
     * his method takes a Coordinate, and sees which (if any) Ship
     * occupies that coordinate. If one is found, we return whatever
     * displayInfo it has at those coordinates (for now, just 's'). If
     * none is found, we return null.
     * 
     * @param where is the input coordinate
     * @return
     */
    public T whatIsAtForSelf(Coordinate where) {
        return whatIsAt(where, true);
    }

    /**
     * Return the enemy check.
     */
    public T whatIsAtForEnemy(Coordinate where) {
        return whatIsAt(where, false);
    }

    /**
     * return the message of the coordinate, and also check if the coordinate input
     * is valid.
     * 
     * @param where  The coordinate to check.
     * @param isSelf if it is my board.
     * @return returns the information
     */
    protected T whatIsAt(Coordinate where, boolean isSelf) {
        if (where.getColumn() > width - 1 || where.getRow() > height - 1) {
            throw new IllegalArgumentException("The coordinate is out of bound in board!");
        }

        for (Ship<T> s : myShips) {
            if (isSelf) {
                if (s.occupiesCoordinates(where)) { // changed???
                    return s.getDisplayInfoAt(where, isSelf);
                }
            } else {
                if (s.occupiesCoordinates(where) && hitHistory.containsKey(where) && !enemyMisses.contains(where)) { // changed???
                    return s.getDisplayInfoAt(where, isSelf);
                }
            }
        }
        if (!isSelf && enemyMisses.contains(where)) {
            return missInfo;
        }
        if (hitHistory.containsKey(where) && !isSelf) {
            return (T) hitHistory.get(where);
        }
        // else if (hitHistory.containsKey(where) && !isSelf){
        // return (T) hitHistory.get(where);
        // }

        return null;
    }

    /**
     * Fire at the current coordinate.
     * 
     * @param c is the coordinate being fired at.
     * @return return the ship
     */
    public Ship<T> fireAt(Coordinate c) {
        for (Ship<T> s : myShips) {
            if (s.occupiesCoordinates(c)) {
                s.recordHitAt(c);
                if (!hitHistory.containsKey(c)) {
                    hitHistory.put(c, Character.toLowerCase(s.getName().charAt(0)));
                    enemyMisses.remove(c);
                }
                for (Coordinate missedCoordinate : enemyMisses) {
                    if (missedCoordinate.equals(c)) {
                        enemyMisses.remove(missedCoordinate);
                    }
                }
                return s;
            }
        }
        enemyMisses.add(c);
        return null;
    }

    /**
     * Check if all the ships are sunk
     */
    public boolean checkAllSunk() {
        for (Ship<T> myShip : this.myShips) {
            if (!myShip.isSunk()) {
                return false;
            }
        }
        return true;
    }

    /**
     * Add ship to myShips array list method
     * 
     * @param toAdd is the ship to be added to the array list
     * @return the validity of the placement
     */
    public String tryAddShipExact(Ship<T> toAdd, int index) {
        String message = placementChecker.checkPlacement(toAdd, this);
        if (message == null) {
            myShips.add(index, toAdd);
        }
        return message;
    }

    public void makeMove(Ship<T> oldShip, Placement newPlacement) {
        int oldIndex = myShips.indexOf(oldShip);

        Ship<T> shipToMove = oldShip;

        myShips.remove(oldShip);
        Ship<Character> newShip = null;

        String shipType = shipToMove.getName();
        V2ShipFactory factory = new V2ShipFactory();

        if (shipType.equals("Carrier") || shipType.equals("Battleship")) {
            if (shipType.equals("Carrier")) {
                newShip = factory.makeCarrier(newPlacement);
            } else {
                newShip = factory.makeBattleship(newPlacement);
            }
        } else {
            if (shipType.equals("Submarine")) {
                newShip = factory.makeSubmarine(newPlacement);
            } else {
                newShip = factory.makeDestroyer(newPlacement);
            }
        }
        newShip.copyHitInfo((Ship<Character>) shipToMove);
        String message = tryAddShipExact((Ship<T>) newShip, oldIndex);
        if (message != null) {
            throw new IllegalArgumentException(message);
        }
        return;
    }

    public ArrayList<Ship<T>> getMyShip() {
        return this.myShips;
    }

    public int[] detect(Board<Character> enemyBoard, Coordinate c) {
        int ans[] = { 0, 0, 0, 0 };
        int startCol = c.getColumn();
        int startRow = c.getRow();

        for (int i = startRow - 3; i <= startRow + 3; i++) {
            for (int j = startCol - 3; j <= startCol + 3; j++) {
                if (Math.abs(i - startRow) + Math.abs(j - startCol) <= 3) {
                    int actualRow = i;
                    int actualCol = j;
                    if ((actualRow >= 0 && actualRow < enemyBoard.getHeight()) && (actualCol >= 0 && actualCol < enemyBoard.getWidth())) {
                        for (Ship<T> ship : myShips) {
                            if (ship.occupiesCoordinates(new Coordinate(actualRow, actualCol))) {
                                String message = ship.getName();
                                if (message.equals("Submarine")) {
                                    ans[0]++;
                                } else if (message.equals("Destroyer")) {
                                    ans[1]++;
                                } else if (message.equals("Battleship")) {
                                    ans[2]++;
                                } else if (message.equals("Carrier")) {
                                    ans[3]++;
                                }
                            }
                        }
                    }

                }
            }
        }

        return ans;
    }

}
