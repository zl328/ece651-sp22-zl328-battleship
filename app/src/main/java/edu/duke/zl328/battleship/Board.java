package edu.duke.zl328.battleship;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Board
 */
public interface Board<T> {
    public int getWidth();

    public int getHeight();

    public String tryAddShip(Ship<T> toAdd);

    public String tryAddShipExact(Ship<T> toAdd, int index);

    public T whatIsAtForSelf(Coordinate where);

    public Ship<T> fireAt(Coordinate c);

    public T whatIsAtForEnemy(Coordinate where);

    public boolean checkAllSunk();

    public void makeMove(Ship<T> shipToMove, Placement newPlacement);

    public ArrayList<Ship<T>> getMyShip();

    public int[] detect(Board<Character> enemyBoard, Coordinate c);
}