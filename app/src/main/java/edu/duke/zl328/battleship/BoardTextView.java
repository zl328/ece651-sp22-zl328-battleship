package edu.duke.zl328.battleship;

import java.util.function.Function;

/**
 * This class ahndles textual display of a
 * board (ie., converting it to a string to
 * show to the user).
 * It supports two ways to display the Board:
 * one for the player's own board, and one
 * for the enemy's board.
 */
public class BoardTextView {
    /**
     * The Board to display
     */
    private final Board<Character> toDisplay;

    /**
     * Constructs a BoardView, given the board
     * it will display.
     * 
     * @param toDisplay is the Board to display.
     * @throws IllegalArgumentException if the board is larger than 10x26
     */
    public BoardTextView(Board<Character> toDisplay) {
        this.toDisplay = toDisplay;
        if (toDisplay.getWidth() > 10 || toDisplay.getHeight() > 26) {
            throw new IllegalArgumentException(
                    "Board must be no larger than 10x26, but is " + toDisplay.getWidth() + "x" + toDisplay.getHeight());
        }
    }

    /**
     * the display message for my own board
     * 
     * @return the info in string
     */
    public String displayMyOwnBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForSelf(c));
    }

    /**
     * the display message for enemy board
     * 
     * @return the info in string
     */
    public String displayEnemyBoard() {
        return displayAnyBoard((c) -> toDisplay.whatIsAtForEnemy(c));
    }

    /**
     * display the board for any given getSquareFn
     * 
     * @param getSquareFn my board or enemy's board
     * @return info in string
     */
    protected String displayAnyBoard(Function<Coordinate, Character> getSquareFn) {
        StringBuilder sb = new StringBuilder();
        sb.append(makeHeader());
        for (int row = 0; row < toDisplay.getHeight(); row++) {
            sb.append((char) ('A' + row));
            sb.append(" ");
            sb.append(getSquareFn.apply(new Coordinate(row, 0)) == null ? " "
                    : getSquareFn.apply(new Coordinate(row, 0)));
            for (int col = 1; col < toDisplay.getWidth(); col++) {
                sb.append("|");
                sb.append(
                        getSquareFn.apply(new Coordinate(row, col)) == null ? " "
                                : getSquareFn.apply(new Coordinate(row, col)));
            }
            sb.append(" ");
            sb.append((char) ('A' + row));
            sb.append("\n");
        }
        sb.append(makeHeader());
        return sb.toString();
    }

    /**
     * This makes the header line, e.g. 0|1|2|3|4\n
     * 
     * @return the String that is the header line for the given board
     */
    String makeHeader() {
        StringBuilder ans = new StringBuilder(" "); // READEME shows two spaces at
        String sep = " "; // start with nothing to seperate, then switch to | to seperate
        for (int i = 0; i < toDisplay.getWidth(); i++) {
            ans.append(sep);
            ans.append(i);
            sep = "|";
        }
        ans.append("\n");
        return ans.toString();
    }

    /**
     * generate empty string with white space
     * 
     * @param c     what to fill with
     * @param times how many character need to generate
     * @return the string
     */
    private String fillWthSpace(Character c, int times) {
        StringBuilder ans = new StringBuilder("");
        for (int i = 0; i < times; i++) {
            ans.append(c);
        }
        return ans.toString();
    }

    /**
     * display my board and enemy's board side by side
     * 
     * @param enemyView   the enemy's view
     * @param myHeader    the header string of me
     * @param enemyHeader the header string of enemy
     * @return the formatted string of two boards
     */
    public String displayMyBoardWithEnemyNextToIt(BoardTextView enemyView, String myHeader, String enemyHeader) {
        int width = 2 * toDisplay.getWidth() + 24;
        int width2 = 2 * toDisplay.getWidth() + 19;
        // title length
        int headerStartLength = 5;
        StringBuilder ans = new StringBuilder("");
        ans.append(fillWthSpace(' ', headerStartLength));
        ans.append(myHeader);
        int headerMidLength = width - (headerStartLength + myHeader.length());
        ans.append(fillWthSpace(' ', headerMidLength));
        ans.append(enemyHeader);
        ans.append("\n");
        // header length
        String[] myDisplay = displayMyOwnBoard().split("\n");
        String[] enenemyDisplay = enemyView.displayEnemyBoard().split("\n");
        for (int i = 0; i < myDisplay.length; i++) {
            ans.append(myDisplay[i]);
            ans.append(fillWthSpace(' ', width2 - myDisplay[i].length()));
            ans.append(enenemyDisplay[i]);
            ans.append("\n");
        }
        return ans.toString();
    }

}
