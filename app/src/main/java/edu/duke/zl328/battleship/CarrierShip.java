package edu.duke.zl328.battleship;

import java.util.ArrayList;
import java.util.HashSet;

public class CarrierShip<T> extends BasicShip<T> {
    /**
     * make the carrier
     * 
     * @param coordinate  is the upperleft coordinate
     * @param orientation is the orientation of the ship
     * @return the hashset of coordinates
     */
    public static Iterable<Coordinate> makeCoord(Coordinate coordinate, Character orientation) {
        HashSet<Coordinate> newShip = new HashSet<>();
        int startCol = coordinate.getColumn();
        int startRow = coordinate.getRow();
        if (orientation == 'U') {
            // first column
            newShip.add(new Coordinate(startRow + 3, startCol));
            newShip.add(new Coordinate(startRow + 2, startCol));
            newShip.add(new Coordinate(startRow + 1, startCol));
            newShip.add(new Coordinate(startRow, startCol));
            // second column
            newShip.add(new Coordinate(startRow + 4, startCol + 1));
            newShip.add(new Coordinate(startRow + 3, startCol + 1));
            newShip.add(new Coordinate(startRow + 2, startCol + 1));
        } else if (orientation == 'D') {
            newShip.add(new Coordinate(startRow + 1, startCol + 1));
            newShip.add(new Coordinate(startRow + 2, startCol + 1));
            newShip.add(new Coordinate(startRow + 3, startCol + 1));
            newShip.add(new Coordinate(startRow + 4, startCol + 1));
            // second column
            newShip.add(new Coordinate(startRow, startCol));
            newShip.add(new Coordinate(startRow + 1, startCol));
            newShip.add(new Coordinate(startRow + 2, startCol));
        } else if (orientation == 'R') {
            // first column
            newShip.add(new Coordinate(startRow, startCol + 1));
            newShip.add(new Coordinate(startRow, startCol + 2));
            newShip.add(new Coordinate(startRow, startCol + 3));
            newShip.add(new Coordinate(startRow, startCol + 4));
            // second column
            newShip.add(new Coordinate(startRow + 1, startCol));
            newShip.add(new Coordinate(startRow + 1, startCol + 1));
            newShip.add(new Coordinate(startRow + 1, startCol + 2));
        } else if (orientation == 'L') {
            // first column
            newShip.add(new Coordinate(startRow + 1, startCol + 3));
            newShip.add(new Coordinate(startRow + 1, startCol + 2));
            newShip.add(new Coordinate(startRow + 1, startCol + 1));
            newShip.add(new Coordinate(startRow + 1, startCol));
            // second column
            newShip.add(new Coordinate(startRow, startCol + 4));
            newShip.add(new Coordinate(startRow, startCol + 3));
            newShip.add(new Coordinate(startRow, startCol + 2));
        }
        return newShip;
    }

    /**
     * store the coordinates of the ship in a specific sequence
     * 
     * @param coordinate  is the upperleft of the coordinates
     * @param orientation is the orientation of the ship
     * @return the arraylist of coordinates
     */
    public static ArrayList<Coordinate> makeIndex(Coordinate coordinate, Character orientation) {
        ArrayList<Coordinate> newShip = new ArrayList<Coordinate>();
        int startCol = coordinate.getColumn();
        int startRow = coordinate.getRow();
        if (orientation == 'U') {
            // first column
            newShip.add(new Coordinate(startRow + 3, startCol));
            newShip.add(new Coordinate(startRow + 2, startCol));
            newShip.add(new Coordinate(startRow + 1, startCol));
            newShip.add(new Coordinate(startRow, startCol));
            // second column
            newShip.add(new Coordinate(startRow + 4, startCol + 1));
            newShip.add(new Coordinate(startRow + 3, startCol + 1));
            newShip.add(new Coordinate(startRow + 2, startCol + 1));
        } else if (orientation == 'D') {
            newShip.add(new Coordinate(startRow + 1, startCol + 1));
            newShip.add(new Coordinate(startRow + 2, startCol + 1));
            newShip.add(new Coordinate(startRow + 3, startCol + 1));
            newShip.add(new Coordinate(startRow + 4, startCol + 1));
            // second column
            newShip.add(new Coordinate(startRow, startCol));
            newShip.add(new Coordinate(startRow + 1, startCol));
            newShip.add(new Coordinate(startRow + 2, startCol));
        } else if (orientation == 'R') {
            // first column
            newShip.add(new Coordinate(startRow, startCol + 1));
            newShip.add(new Coordinate(startRow, startCol + 2));
            newShip.add(new Coordinate(startRow, startCol + 3));
            newShip.add(new Coordinate(startRow, startCol + 4));
            // second column
            newShip.add(new Coordinate(startRow + 1, startCol));
            newShip.add(new Coordinate(startRow + 1, startCol + 1));
            newShip.add(new Coordinate(startRow + 1, startCol + 2));
        } else if (orientation == 'L') {
            // first column
            newShip.add(new Coordinate(startRow + 1, startCol + 3));
            newShip.add(new Coordinate(startRow + 1, startCol + 2));
            newShip.add(new Coordinate(startRow + 1, startCol + 1));
            newShip.add(new Coordinate(startRow + 1, startCol));
            // second column
            newShip.add(new Coordinate(startRow, startCol + 4));
            newShip.add(new Coordinate(startRow, startCol + 3));
            newShip.add(new Coordinate(startRow, startCol + 2));
        }
        return newShip;
    }

    /**
     * the constructor of carrier
     * 
     * @param where is the position
     * @param data  is the data
     * @param onHit is the onhit
     */

    public CarrierShip(Placement where, T data, T onHit) {
        this(where, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));

    }

    /**
     * the constructor of carrier
     * 
     * @param where is the position
     * @param data  is the data
     * @param onHit is the onhit
     */
    public CarrierShip(Placement where, ShipDisplayInfo<T> m, ShipDisplayInfo<T> n) {
        super(makeCoord(where.getWhere(), where.getOrientation()), makeIndex(where.getWhere(), where.getOrientation()),
                m, n);
    }

    /**
     * get the name of the ship
     */
    @Override
    public String getName() {
        return "Carrier";
    }

}
