package edu.duke.zl328.battleship;

/**
 * Coordinate class, used to generate the
 * coordinates
 */
public class Coordinate {
    private final int row;
    private final int column;

    /**
     * Coordinate constructor using row and column number
     * 
     * @param r indicates the number of row
     * @param c indicates the number of column
     */
    public Coordinate(int r, int c) {
        this.row = r;
        this.column = c;
    }

    /**
     * Coordinate constructor using a string.
     * 
     * @param descr indicates the exact coordinate(eg. A2).
     * @throws IllegalArgumentException when row number is not in the
     *                                  range of 'a' - 'z' or 'A' - 'Z'.
     */
    public Coordinate(String descr) {
        if (descr.length() != 2) {
            throw new IllegalArgumentException("Wrong coordinate input format. Illegal length!");
        }

        int column = Character.getNumericValue(descr.charAt(1));
        int rowLetter = descr.charAt(0);
        int row = 0;

        if (!Character.isDigit(descr.charAt(1))) {
            throw new IllegalArgumentException("Wrong coordinate input format. ILlegal column number!");
        }

        if (rowLetter >= 'A' && rowLetter <= 'Z') {
            row = rowLetter - 'A';
        } else if (rowLetter >= 'a' && rowLetter <= 'z') {
            row = rowLetter - 'a';
        } else {
            throw new IllegalArgumentException("Wrong coordinate input format. ILlegal row number!");
        }
        this.row = row;
        this.column = column;
    }

    /**
     * getter for row
     * 
     * @return row number
     */
    public int getRow() {
        return this.row;
    }

    /**
     * getter for column
     * 
     * @return column number
     */
    public int getColumn() {
        return this.column;
    }

    /**
     * overridden function of equals. Check
     * if two Coordinates are the same.
     * 
     * @param the Coordinate to compare.
     * @return a boolean of true or false
     */
    @Override
    public boolean equals(Object o) {
        if (o.getClass().equals(getClass())) {
            Coordinate c = (Coordinate) o;
            return row == c.row && column == c.column;
        }
        return false;
    }

    /**
     * Hash the string
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * row and column in string format
     */
    @Override
    public String toString() {
        return "(" + row + ", " + column + ")";
    }
}
