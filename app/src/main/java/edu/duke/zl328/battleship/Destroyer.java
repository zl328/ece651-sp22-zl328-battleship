package edu.duke.zl328.battleship;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

public class Destroyer<T> extends BasicShip<T> {
    /**
     * make the destroyer
     * 
     * @param coordinate  is the upperleft coordinate
     * @param orientation is the orientation of the ship
     * @return the hashset of coordinates
     */
    public static Iterable<Coordinate> makeCoord(Coordinate coordinate, Character orientation) {
        HashSet<Coordinate> coordinateHashSet = new HashSet<Coordinate>();
        int r = coordinate.getRow();
        int c = coordinate.getColumn();
        // destroyer
        // verticle
        if (orientation == 'V') {
            coordinateHashSet.add(new Coordinate(r, c));
            coordinateHashSet.add(new Coordinate(r + 1, c));
            coordinateHashSet.add(new Coordinate(r + 2, c));
        }
        // horizontal
        else {
            coordinateHashSet.add(new Coordinate(r, c));
            coordinateHashSet.add(new Coordinate(r, c + 1));
            coordinateHashSet.add(new Coordinate(r, c + 2));
        }

        return coordinateHashSet;

    }

    /**
     * store the coordinates of the ship in a specific sequence
     * 
     * @param coordinate  is the upperleft of the coordinates
     * @param orientation is the orientation of the ship
     * @return the arraylist of coordinates
     */
    public static ArrayList<Coordinate> makeIndex(Coordinate coordinate, Character orientation) {
        ArrayList<Coordinate> coordinateHashSet = new ArrayList<Coordinate>();
        int r = coordinate.getRow();
        int c = coordinate.getColumn();
        // destroyer
        // verticle
        if (orientation == 'V') {
            coordinateHashSet.add(new Coordinate(r, c));
            coordinateHashSet.add(new Coordinate(r + 1, c));
            coordinateHashSet.add(new Coordinate(r + 2, c));
        }
        // horizontal
        else {
            coordinateHashSet.add(new Coordinate(r, c));
            coordinateHashSet.add(new Coordinate(r, c + 1));
            coordinateHashSet.add(new Coordinate(r, c + 2));
        }

        return coordinateHashSet;
    }

    /**
     * the constructor of destroyer
     * 
     * @param where is the position
     * @param data  is the data
     * @param onHit is the onhit
     */
    public Destroyer(Placement where, T data, T onHit) {
        this(where, new SimpleShipDisplayInfo<T>(data, onHit), new SimpleShipDisplayInfo<T>(null, data));

    }

    /**
     * the constructor of destroyer
     * 
     * @param where is the position
     * @param data  is the data
     * @param onHit is the onhit
     */
    public Destroyer(Placement where, ShipDisplayInfo<T> m, ShipDisplayInfo<T> n) {
        super(makeCoord(where.getWhere(), where.getOrientation()), makeIndex(where.getWhere(), where.getOrientation()),
                m, n);
    }

    /**
     * get the name of the ship
     */
    @Override
    public String getName() {
        return "Destroyer";
    }

}
