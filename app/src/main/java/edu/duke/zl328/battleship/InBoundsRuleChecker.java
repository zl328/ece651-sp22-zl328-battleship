package edu.duke.zl328.battleship;

public class InBoundsRuleChecker<T> extends PlacementRuleChecker<T> {

    /**
     * Constructor of Inbounds Rule Checker.
     * 
     * @param next next object to be checked.
     */
    public InBoundsRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * Check if the placements are in bounds.
     * 
     * @param theShip  is the Ship to be checked.
     * @param theBoard is the board to be checked.
     * @return the message. If valid return null, else return error message.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        int height = theBoard.getHeight();
        int width = theBoard.getWidth();

        for (Coordinate c : theShip.getCoordinates()) {
            if (c.getRow() < 0) {
                return "That placement is invalid: the ship goes off the top of the board.";
            }
            if (c.getColumn() < 0) {
                return "That placement is invalid: the ship goes off the left of the board.";
            }
            if (c.getRow() >= height) {
                return "That placement is invalid: the ship goes off the bottom of the board.";
            }
            if (c.getColumn() >= width) {
                return "That placement is invalid: the ship goes off the right of the board.";
            }

        }
        return null;
    }
}
