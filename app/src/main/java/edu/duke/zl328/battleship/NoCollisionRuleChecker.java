package edu.duke.zl328.battleship;

public class NoCollisionRuleChecker<T> extends PlacementRuleChecker<T> {
    /**
     * Constructor of No Collision Rule Checker.
     * 
     * @param next the next object to be checked.
     */
    public NoCollisionRuleChecker(PlacementRuleChecker<T> next) {
        super(next);
    }

    /**
     * Check if the placement does not overlap another.
     * 
     * @param theShip  is the Ship to be checked.
     * @param theBoard is the board to be checked.
     * @return the message. If valid return null, else return error message.
     */
    @Override
    protected String checkMyRule(Ship<T> theShip, Board<T> theBoard) {
        for (Coordinate coordinate : theShip.getCoordinates()) {
            if (theBoard.whatIsAtForSelf(coordinate) != null) {
                return "That placement is invalid: the ship overlaps another ship.";
            }
        }
        return null;
    }

}
