package edu.duke.zl328.battleship;

/**
 * placement on the Board
 */
public class Placement {
    private final Coordinate where;
    private final char orientation;

    /**
     * placement using seperate coordinate and orientation
     * 
     * @param where       place to a specific coordinate
     * @param orientation the orientation of your placememt. All parsed to capital
     *                    letter
     *                    i.e. new Placement(a2, 'v')
     * check if the orientation is not v, h, u, d, l
     */
    public Placement(Coordinate w, char o) {
        this.where = w;
        this.orientation = Character.toUpperCase(o);
        if (this.orientation != 'V' && this.orientation != 'H' && this.orientation != 'U' && this.orientation != 'D' && this.orientation != 'L' && this.orientation != 'R') {
            throw new IllegalArgumentException("Wrong coordinate input format. Illegal row number!");
        }
    }

    /**
     * placement using a string
     * 
     * @param descr including coordinates and orientation.
     *              i.e. new Placement(A2V)
     */
    public Placement(String descr) {
        descr = descr.toUpperCase();
        if (descr.length() != 3) {
            throw new IllegalArgumentException("Wrong coordinate input format. ILlegal row number!");
        }
        if (descr.charAt(2) != 'V' && descr.charAt(2) != 'H' && descr.charAt(2) != 'U' && descr.charAt(2) != 'D' && descr.charAt(2) != 'L' && descr.charAt(2) != 'R') {
            throw new IllegalArgumentException("Wrong coordinate input format. ILlegal row number!");
        }
        this.where = new Coordinate(descr.substring(0, 2));
        this.orientation = descr.charAt(2);

    }

    /**
     * getter for orientation
     * 
     * @return the orientation
     */
    public char getOrientation() {
        return orientation;
    }

    /**
     * getter for coordinate
     * 
     * @return the coordinate
     */
    public Coordinate getWhere() {
        return where;
    }

    /**
     * return the message in string
     */
    @Override
    public String toString() {
        return this.where.toString() + " facing " + this.orientation;
    }

    /**
     * check if two placemens are equal
     */
    @Override
    public boolean equals(Object obj) {
        if (obj.getClass().equals(getClass())) {
            Placement p = (Placement) obj;
            return p.orientation == orientation && p.where.equals(where);
        }
        return false;
    }

    /**
     * return hash code of the message
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }
}
