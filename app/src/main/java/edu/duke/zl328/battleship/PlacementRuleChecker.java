package edu.duke.zl328.battleship;

public abstract class PlacementRuleChecker<T> {
    private final PlacementRuleChecker<T> next;

    /**
     * @Description: Abstract constructor of PlacementRuleChecker
     * @Param: PlacementRuleChecker<T> next
     * @return:
     **/
    protected PlacementRuleChecker(PlacementRuleChecker<T> next) {
        this.next = next;
    }

    /**
     * @Description: Check if the passed ship can be placed on the passed board
     *               based on special standard
     * @Param: Ship<T> theShip, Board<T> theBoard
     * @return: Boolean that indicate whether the passed ship can be placed on the
     *          passed board
     **/
    protected abstract String checkMyRule(Ship<T> theShip, Board<T> theBoard);

    /**
     * @Description: Check if the passed ship can be placed on the passed board
     *               based on every designed standard
     * @Param: Ship<T> theShip, Board<T> theBoard
     * @return: Boolean that indicate whether the passed ship can be placed on the
     *          passed board
     **/
    public String checkPlacement(Ship<T> theShip, Board<T> theBoard) {
        // if we fail our own rule: stop the placement is not legal
        String message = checkMyRule(theShip, theBoard);
        if (message != null) {
            return message;
        }
        // other wise, ask the rest of the chain.
        if (next != null) {
            return next.checkPlacement(theShip, theBoard);
        }
        // if there are no more rules, then the placement is legal
        return null;
    }
}
