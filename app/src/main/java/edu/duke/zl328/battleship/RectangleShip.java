package edu.duke.zl328.battleship;

import java.util.ArrayList;
import java.util.HashSet;

public class RectangleShip<T> extends BasicShip<T> {
	final String shipName;

	/**
	 * makes the the coordinate set based on the initial coordinate and the given
	 * width and height
	 * of one single ship.
	 * 
	 * @param upperLeft is the upperleft corner of the ship
	 * @param width     is the width of the ship
	 * @param height    is the height of the ship
	 * @return a collection of coordinates of the single ship
	 */

	public static HashSet<Coordinate> makeCoords(Coordinate upperLeft, int width, int height) {
		HashSet<Coordinate> coordinateHashSet = new HashSet<Coordinate>();

		int r = upperLeft.getRow();
		int c = upperLeft.getColumn();
		for (int i = r; i < height + r; i++) {
			for (int j = c; j < width + c; j++) {
				Coordinate n = new Coordinate(i, j);
				coordinateHashSet.add(n);
			}
		}
		return coordinateHashSet;
	}

	/**
	 * RectangleShip constructor
	 * 
	 * @param name             the name of the ship
	 * @param upperLeft        the coordinate of the upper left corner
	 * @param width            the width of the ship
	 * @param height           the height of the ship
	 * @param myDisplayInfo    the display info if it is my ship
	 * @param enemyDisplayInfo the display info if it is enemy ship
	 */
	public RectangleShip(String name, Coordinate upperLeft, int width, int height, ShipDisplayInfo<T> myDisplayInfo,
			ShipDisplayInfo<T> enemyDisplayInfo) {
		super(makeCoords(upperLeft, width, height), makeCoords(upperLeft, width, height), myDisplayInfo,
				enemyDisplayInfo);
		this.shipName = name;
	}

	/**
	 * Rectangle Ship constructor
	 * 
	 * @param name      the name of the ship
	 * @param upperLeft the coordinate of the upper left corner
	 * @param width     the width of the ship
	 * @param height    the height of the ship
	 * @param data      the return message of the ship
	 * @param onHit     if the ship has been hit
	 */
	public RectangleShip(String name, Coordinate upperLeft, int width, int height, T data, T onHit) {
		this(name, upperLeft, width, height, new SimpleShipDisplayInfo<T>(data, onHit),
				new SimpleShipDisplayInfo<T>(null, data));
	}

	/**
	 * Rectangle Ship constructor with the default name "testship"
	 * 
	 * @param upperLeft the coordinate of the upper left corner
	 * @param data      the return message of the ship
	 * @param onHit     if the ship has been hit
	 */
	public RectangleShip(Coordinate upperLeft, T data, T onHit) {
		this("testship", upperLeft, 1, 1, data, onHit);
	}

	public String getName() {
		return this.shipName;
	}

}
