package edu.duke.zl328.battleship;

public interface ShipDisplayInfo<T> {
    
    /**
     * This gets the info (with type T) of ship
     * @param where is the Coordinate to check if this Ship occupies
     * @param hit   is boolean to indicate if the ship is hit or not
     */

    public T getInfo(Coordinate where, boolean hit);
}
