package edu.duke.zl328.battleship;

public class SimpleShipDisplayInfo<T> implements ShipDisplayInfo<T> {
    T myData;
    T onHit;

    /**
     * Constructor of ShipDisplayInfo
     * 
     * @param myData
     * @param onHit
     */
    public SimpleShipDisplayInfo(T myData, T onHit) {
        this.myData = myData;
        this.onHit = onHit;
    }

    /**
     * return the display info of the ship
     */
    @Override
    public T getInfo(Coordinate where, boolean hit) {

        if (hit) {
            return onHit;
        } else {
            return myData;
        }
    }

}
