package edu.duke.zl328.battleship;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.function.Function;

public class TextPlayer {
    final Board<Character> theBoard;
    final BoardTextView view;
    final BufferedReader inputReader;
    final PrintStream out;
    final AbstractShipFactory<Character> shipFactory;
    final String name;
    final ArrayList<String> shipsToPlace;
    final HashMap<String, Function<Placement, Ship<Character>>> shipCreationFns;
    protected int moveTimes;
    protected int sonarTimes;
    protected int aiHitRow;
    protected int aiHitColumn;

    /**
     * The text player Constructor
     * 
     * @param name        is the name of the player
     * @param theBoard    is the board of the player
     * @param inputReader is the input method
     * @param out         is the out method
     * @param shipFactory is the ship factory that makes rectangular ship
     */
    public TextPlayer(String name, Board<Character> theBoard, BufferedReader inputReader, PrintStream out,
            AbstractShipFactory<Character> shipFactory) {
        this.theBoard = theBoard;
        this.view = new BoardTextView(theBoard);
        this.inputReader = inputReader;
        this.out = out;
        this.shipFactory = shipFactory;
        this.name = name;
        this.shipsToPlace = new ArrayList<String>();
        this.shipCreationFns = new HashMap<String, Function<Placement, Ship<Character>>>();
        setupShipCreationMap();
        setupShipCreationList();
        moveTimes = 2;
        sonarTimes = 1;
        aiHitRow = 0;
        aiHitColumn = 0;
    }

    /**
     * read the placement from the reader. Repeat until the
     * correct format
     * 
     * @param prompt the string of prompt
     * @return the correct placement
     * @throws IOException if player input wrong format
     */
    public Placement readPlacement(String prompt) throws IOException {
        String result = null;
        do {
            try {
                out.println(prompt);
                result = inputReader.readLine();
                return new Placement(result);
            } catch (IllegalArgumentException e) {
                out.println(e + " Please try again!");
            }

        } while (true);
    }

    /**
     * Do single placement for a user
     * 
     * @param shipName is the ship to be placed
     * @param createFn is the createFunction of the ship
     * @throws IOException if no ship can be placed at the coordinate
     */
    public void doOnePlacement(String shipName, Function<Placement, Ship<Character>> createFn) throws IOException {
        do {
            try {
                Placement p = readPlacement("\nPlayer " + name + " where do you want to place a " + shipName + "?");
                Ship<Character> s = createFn.apply(p);
                String message = theBoard.tryAddShip(s);
                if (message != null) {
                    out.print("\n" + message + " Please enter again!\n");
                    continue;
                }
                break;
            } catch (IllegalArgumentException e) {
                out.print(e);
            }

        } while (true);
        out.println("\nCurrent ocean:");
        out.println(view.displayMyOwnBoard());
    }

    /**
     * handles all the placements of for the player
     * 
     * @throws IOException if there's invalid placement
     */
    public boolean doPlacementPhase() throws IOException {
        out.println("\n\n\nWelcome to the ANNUL ULTIMATE BATTLESHIP CONTEST\n\n\n");
        out.println("ARE U READY!!!!!!! Lets get SOOOOOOOOOME" +
                "NOOOOOOOOOOOOOOOOOOOISE!!!!!!!!!!!\n\n\n");
        do {
            out.println("Do you want a computer to play for you? (enter y or n)");
            String ans = inputReader.readLine().toUpperCase();
            if (ans.equals("Y")) {
                autoPlace();
                return true;
            } else if (ans.equals("N")) {
                break;
            } else {
                out.println("Invalid option. Please enter again!");
                continue;
            }
        } while (true);

        out.println(view.displayMyOwnBoard());
        StringBuilder prompt = new StringBuilder("");
        prompt.append("Player " + this.name + ":");
        prompt.append(
                " you are going to place the following ships (which are all rectangular). For"
                        + "each ship, type the coordinate of the upper left side of the ship, followed"
                        + "by either H (for horizontal) or V (for vertical). For example M4H would place"
                        + "a ship horizontally starting at M4 and going to the right. You have\n\n"
                        + "2 \"Submarines\" ships that are 1x2\n" + "3 \"Destroyers\" that are 1x3\n"
                        + "3 \"Battleships\" that are 1x4\n" + "2 \"Carriers\" that are 1x6\n");
        out.print(prompt.toString());
        for (String ship : this.shipsToPlace) {
            doOnePlacement(ship, this.shipCreationFns.get(ship));
        }
        out.print("\n\n");
        return false;
    }

    /**
     * map the ship create function to the name of the ship
     */
    protected void setupShipCreationMap() {
        shipCreationFns.put("Submarine", (p) -> shipFactory.makeSubmarine(p));
        shipCreationFns.put("Destroyer", (p) -> shipFactory.makeDestroyer(p));
        shipCreationFns.put("Battleship", (p) -> shipFactory.makeBattleship(p));
        shipCreationFns.put("Carrier", (p) -> shipFactory.makeCarrier(p));
    }

    /**
     * set up all the name of the ships to be created
     */
    protected void setupShipCreationList() {
        shipsToPlace.addAll(Collections.nCopies(2, "Submarine"));
        shipsToPlace.addAll(Collections.nCopies(3, "Destroyer"));
        shipsToPlace.addAll(Collections.nCopies(3, "Battleship"));
        shipsToPlace.addAll(Collections.nCopies(2, "Carrier"));
    }

    /**
     * Handles all the attack phase of one player
     * 
     * @param enemyBoard is the enemy's board
     * @param enemyView  is the enemy's view
     * @param enemyName  is the name of the enemy player
     * @throws IOException if fire at the wrong coordinate
     */
    public void playOneTurn(Board<Character> enemyBoard, BoardTextView enemyView, String enemyName) throws IOException {
        out.print("Player " + this.name + "'s turn:\n");
        out.print(
                view.displayMyBoardWithEnemyNextToIt(enemyView, "\'Your ocean\'", "Player " + enemyName + "'s ocean"));

        String choice;
        do {
            out.println("\n\nPossible actions for Player " + this.name + ": \n");
            out.println("  F Fire at a square");
            out.println("  M Move a ship to another square (" + moveTimes + " remaining)");
            out.println("  S Sonar scan (" + sonarTimes + " remaining)");
            choice = inputReader.readLine().toUpperCase();
            if (!choice.equals("M") && !choice.equals("F") && !choice.equals("F")) {
                if ((choice.equals("M") && moveTimes <= 0) || (choice.equals("S") && sonarTimes <= 0)) {
                    out.print("Invalid option! Please try again:");
                    continue;
                }
            }
            if (choice.equals("M")) {
                if (makeAMove()) {
                    moveTimes--;
                    break;
                }
            } else if (choice.equals("F")) {
                if (fireAtShip(enemyBoard)) {
                    break;
                }
            } else if (choice.equals("S")) {
                if (sonarDetect(enemyBoard)) {
                    sonarTimes--;
                    break;
                }
            }
        } while (true);
        return;
    }

    /**
     * the fireAt action
     * 
     * @param enemyBoard the enemy's board to fire at
     * @return true if eveything goes through correctly
     * @throws IOException
     */
    protected boolean fireAtShip(Board<Character> enemyBoard) throws IOException {
        String input = null;
        Coordinate fireCoordinate = null;
        Ship<Character> firedShip = null;
        out.print("Please enter your move:");
        input = inputReader.readLine();
        try {
            fireCoordinate = new Coordinate(input);
            firedShip = enemyBoard.fireAt(fireCoordinate);
        } catch (IllegalArgumentException e) {
            out.println(e + " Please try again!");
            return false;
        }

        if (firedShip == null) {
            out.println("You missed!\n");
        } else {
            out.println("Your hit a " + firedShip.getName() + "!\n");
        }
        return true;
    }

    /**
     * the movement action
     * 
     * @return true if everything goes through correctly
     * @throws IOException
     */
    protected boolean makeAMove() throws IOException {
        StringBuilder choices = new StringBuilder("");
        int shipNumber = 1;
        int userChoice = 0;
        for (Ship<Character> ship : theBoard.getMyShip()) {
            choices.append("Ship" + Integer.toString(shipNumber) + ":" + ship.getName() + "  ");
            shipNumber++;
        }
        out.println("You ship lists: ");
        out.println(choices);

        out.print("Please choose a ship to move:");
        String input = inputReader.readLine();
        try {
            userChoice = Integer.parseInt(input);
            if (userChoice < 1 || userChoice > (theBoard.getMyShip()).size()) {
                out.println("Invalid choice. Please try again!");
                return false;
            }

        } catch (NumberFormatException e) {
            out.println("Input String cannot be parsed to Integer. Please try again!");
            return false;
        }

        userChoice--;
        Ship<Character> oldShip = theBoard.getMyShip().get(userChoice);
        if (oldShip.isSunk()) {
            out.println("You cannot move a sunk ship. Plase try again!");
            return false;
        }

        Placement p;
        try {
            p = readPlacement("\nPlayer " + name + " please enter the new location you want to place:");
        } catch (IllegalArgumentException e) {
            out.print(e);
            return false;
        }

        try {
            theBoard.makeMove(theBoard.getMyShip().get(userChoice), p);

        } catch (IllegalArgumentException e) {
            theBoard.tryAddShipExact(oldShip, userChoice);
            out.print(e);
            return false;
        }
        return true;
    }

    /**
     * do the sonar detection
     * 
     * @param enemyBoard is the enemy's board to detect
     * @return true if everything goes through
     * @throws IOException
     */
    protected boolean sonarDetect(Board<Character> enemyBoard) throws IOException {
        Coordinate c = null;
        out.print("Please select where to scan:");
        String input = inputReader.readLine();
        try {
            c = new Coordinate(input);
            if (c.getColumn() < 0 || c.getColumn() >= theBoard.getWidth() || c.getRow() < 0
                    || c.getRow() >= theBoard.getHeight()) {
                out.println("Invalid placement. Please try again!");
                return false;
            }
        } catch (Exception e) {
            out.println(e + " Please try again!");
            return false;
        }
        int shipCount[] = enemyBoard.detect(enemyBoard, c);
        out.println("Submarines occupy " + shipCount[0] + " squares");
        out.println("Destroyers occupy " + shipCount[1] + " squares");
        out.println("Battleships occupy " + shipCount[2] + " squares");
        out.println("Carriers occupy " + shipCount[3] + " squares");

        return true;
    }

    /**
     * print the win message for player
     * 
     * @param enemyName
     */
    public void winMessage(String enemyName) {
        out.println("\nCongratulations Player " + name + ", you have defeated Player " + enemyName + "!!!");
    }

    /**
     * ai auto place
     */
    public void autoPlace() {
        Ship<Character> sh1 = shipFactory.makeSubmarine(new Placement("a0v"));
        Ship<Character> sh2 = shipFactory.makeSubmarine(new Placement("a1v"));
        Ship<Character> sh3 = shipFactory.makeDestroyer(new Placement("a2v"));
        Ship<Character> sh4 = shipFactory.makeDestroyer(new Placement("a3v"));
        Ship<Character> sh5 = shipFactory.makeDestroyer(new Placement("a4v"));
        Ship<Character> sh6 = shipFactory.makeBattleship(new Placement("a5u"));
        Ship<Character> sh7 = shipFactory.makeBattleship(new Placement("a8l"));
        Ship<Character> sh8 = shipFactory.makeBattleship(new Placement("d0u"));
        Ship<Character> sh9 = shipFactory.makeCarrier(new Placement("d3u"));
        Ship<Character> sh10 = shipFactory.makeCarrier(new Placement("d5u"));
        theBoard.tryAddShip(sh1);
        theBoard.tryAddShip(sh2);
        theBoard.tryAddShip(sh3);
        theBoard.tryAddShip(sh4);
        theBoard.tryAddShip(sh5);
        theBoard.tryAddShip(sh6);
        theBoard.tryAddShip(sh7);
        theBoard.tryAddShip(sh8);
        theBoard.tryAddShip(sh9);
        theBoard.tryAddShip(sh10);
        return;
    }

    /**
     * ai auto attack
     * 
     * @param enemyBoard
     */
    public void autoAttack(Board<Character> enemyBoard) {

        Coordinate hitCoordinate = new Coordinate(aiHitRow, aiHitColumn);
        enemyBoard.fireAt(hitCoordinate);
        aiHitColumn++;
        if (aiHitColumn >= enemyBoard.getWidth()) {
            aiHitColumn = 0;
            aiHitRow++;
        }
        return;
    }
}
