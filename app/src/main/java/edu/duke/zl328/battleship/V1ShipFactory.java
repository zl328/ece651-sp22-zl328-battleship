package edu.duke.zl328.battleship;

// import java.util.HashSet;

public class V1ShipFactory implements AbstractShipFactory<Character> {

    /**
     * make a submarine
     */
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        if(where.getOrientation() != 'V' && where.getOrientation() != 'H') {
            throw new IllegalArgumentException("Wrong orientation!\n");
        }
        return createShip(where, 1, 2, 's', "Submarine");
    }

    /**
     * make a battle ship
     */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        if(where.getOrientation() != 'V' && where.getOrientation() != 'H') {
            throw new IllegalArgumentException("Wrong orientation!\n");
        }
        return createShip(where, 1, 4, 'b', "Battleship");
    }

    /**
     * make a carrier
     */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        return createShip(where, 1, 6, 'c', "Carrier");
    }





    /**
     * make a destroyer
     */
    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        return createShip(where, 1, 3, 'd', "Destroyer");
    }

    /**
     * create the ship of different kinds
     * 
     * @param where    is the coordinate to be placed at
     * @param width    is the width of the ship
     * @param height   is the height of the ship
     * @param shipType is the type of the ship
     * @param message  is the message
     * @return a new ship
     */
    private Ship<Character> createShip(Placement where, int width, int height, char shipType, String message) {
        if (where.getOrientation() == 'H') {
            int temp = width;
            width = height;
            height = temp;
        }

        return new RectangleShip<Character>(message, where.getWhere(), width, height, shipType, '*');
    }

}
