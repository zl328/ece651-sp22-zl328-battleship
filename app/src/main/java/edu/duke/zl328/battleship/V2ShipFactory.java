package edu.duke.zl328.battleship;

public class V2ShipFactory extends V1ShipFactory {
    /**
     * make the submarine
     */
    @Override
    public Ship<Character> makeSubmarine(Placement where) {
        if (where.getOrientation() != 'H' && where.getOrientation() != 'V') {
            throw new IllegalArgumentException("Wrong orientation!\n");
        }
        return new Submarine<Character>(where, 's', '*');
    }

    /**
     * make the destroyer
     */
    @Override
    public Ship<Character> makeDestroyer(Placement where) {
        if (where.getOrientation() != 'H' && where.getOrientation() != 'V') {
            throw new IllegalArgumentException("Wrong orientation!\n");
        }
        return new Destroyer<Character>(where, 'd', '*');
    }

    /**
     * make the battleship
     */
    @Override
    public Ship<Character> makeBattleship(Placement where) {
        if (where.getOrientation() != 'L' && where.getOrientation() != 'R' && where.getOrientation() != 'D'
                && where.getOrientation() != 'U') {
            throw new IllegalArgumentException("Wrong orientation!\n");
        }
        return new BattleShip<Character>(where, 'b', '*');
    }

    /**
     * make the carrier
     */
    @Override
    public Ship<Character> makeCarrier(Placement where) {
        if (where.getOrientation() != 'L' && where.getOrientation() != 'R' && where.getOrientation() != 'D'
                && where.getOrientation() != 'U') {
            throw new IllegalArgumentException("Wrong orientation!\n");
        }
        return new CarrierShip<Character>(where, 'c', '*');
    }
}