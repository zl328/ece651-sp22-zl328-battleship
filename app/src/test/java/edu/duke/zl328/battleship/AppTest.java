package edu.duke.zl328.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

class AppTest {

  @Test
  void test_main() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    PrintStream out = new PrintStream(bytes, true);

    InputStream input = getClass().getClassLoader().getResourceAsStream("input.txt");
    assertNotNull(input);

    InputStream expectedStream = getClass().getClassLoader().getResourceAsStream("output.txt");
    assertNotNull(expectedStream);

    InputStream oldIn = System.in;
    PrintStream oldOut = System.out;

    try {
      System.setIn(input);
      System.setOut(out);
      App.main(new String[0]);
    } finally {
      System.setIn(oldIn);
      System.setOut(oldOut);
    }

    String expected = new String(expectedStream.readAllBytes());
    String actual = bytes.toString();
    assertEquals(expected, actual);

  }

  // BattleSHipBardTest
  private <T> void checkWhatIsAtBoard(BattleShipBoard<T> b, T[][] expected) {
    for (int i = 0; i < b.getHeight(); i++) {
      for (int j = 0; j < b.getWidth(); j++) {
        // assertEquals(expected[i][j], b.whatIsAtForSelf(new Coordinate(i, j)));
        if (i > 5) {
          continue;
        }
        System.out.println("At [" + i + "][" + j + "]: " + b.whatIsAtForSelf(new Coordinate(i, j)));
      }
    }
  }

  @Test
  public void test_width_and_height() {
    Board<Character> b1 = new BattleShipBoard<Character>(10, 20, 'x');
    assertEquals(10, b1.getWidth());
    assertEquals(20, b1.getHeight());
  }

  @Test
  public void test_invalid_dimensions() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, 0, 'x'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(0, 20, 'x'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(10, -5, 'x'));
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-8, 20, 'x'));
  }

  @Test
  public void test_whatIsAtForSelfBoard() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(3, 4, 'x');
    Character[][] expected = new Character[4][3];
    checkWhatIsAtBoard(b1, expected);

    RectangleShip<Character> sh1 = new RectangleShip<Character>(new Coordinate(1, 1), 's', '*');
    RectangleShip<Character> sh2 = new RectangleShip<Character>("Battleship", new Coordinate(3, 2), 2, 1, 's', 'x');
    RectangleShip<Character> sh3 = new RectangleShip<Character>(new Coordinate(0, 2), 's', '*');
    RectangleShip<Character> sh4 = new RectangleShip<Character>("Battleship", new Coordinate(1, 0), 2, 1, 's', 'x');

    assertEquals(null, b1.tryAddShip(sh1));
    assertEquals("That placement is invalid: the ship goes off the right of the board.", b1.tryAddShip(sh2));
    assertEquals(null, b1.tryAddShip(sh3));
    assertEquals("That placement is invalid: the ship overlaps another ship.", b1.tryAddShip(sh4));
    expected[1][1] = null;
    expected[3][2] = null;
    expected[0][2] = null;
    // checkWhatIsAtBoard(b1, expected);
  }

  @Test
  public void test_fire_at() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(3, 4, 'x');
    RectangleShip<Character> sh1 = new RectangleShip<Character>(new Coordinate(1, 2), 's', '*');
    RectangleShip<Character> sh2 = new RectangleShip<Character>(new Coordinate(3, 2), 's', '*');
    b1.tryAddShip(sh1);
    b1.tryAddShip(sh2);
    assertSame(sh1, b1.fireAt(new Coordinate(1, 2)));
    assertSame(true, sh1.isSunk());
    assertEquals(null, b1.fireAt(new Coordinate(5, 6)));
    assertSame(false, sh2.isSunk());
  }

  @Test
  public void check_enemy_miss() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(8, 8, 'X');
    V1ShipFactory f = new V1ShipFactory();
    Coordinate c1 = new Coordinate(1, 2);
    Coordinate c2 = new Coordinate(5, 5);
    Coordinate c3 = new Coordinate(6, 6);
    Placement v1_2 = new Placement(c1, 'V');
    Ship<Character> s = f.makeSubmarine(v1_2);
    b1.tryAddShip(s);
    b1.fireAt(c3);
    assertEquals('X', b1.whatIsAtForEnemy(c3));
    assertEquals(null, b1.whatIsAtForEnemy(c2));
  }

  @Test
  public void check_all_sunk() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 10, 'X');
    RectangleShip<Character> sh1 = new RectangleShip<Character>("Submarine", new Coordinate(0, 1), 1, 1, 's', 'X');
    RectangleShip<Character> sh2 = new RectangleShip<Character>("Submarine", new Coordinate(1, 1), 1, 1, 's', 'X');
    b1.tryAddShip(sh1);
    b1.tryAddShip(sh2);
    b1.fireAt(new Coordinate(1, 1));
    assertEquals(false, b1.checkAllSunk());
    b1.fireAt(new Coordinate(0, 1));
    assertEquals(true, b1.checkAllSunk());
  }

  @Test
  public void check_make_move_carrier() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 10, 'X');
    V2ShipFactory factory = new V2ShipFactory();
    // Placement invalidPlacement = new Placement("a0u");
    Placement oldPlacement = new Placement("a0u");
    Placement a2rPlacement = new Placement("a2r");
    Placement c2dPlacement = new Placement("c2d");
    Placement b5lPlacement = new Placement("b5l");
    Placement invalidPlacement = new Placement("a9u");
    // Ship<Character> sh1 = factory.makeCarrier(invalidPlacement);
    // assertThrows(IllegalArgumentException.class, () -> b1.tryAddShip(sh1));
    Ship<Character> sh2 = factory.makeCarrier(oldPlacement);
    Ship<Character> sh3 = factory.makeCarrier(a2rPlacement);
    Ship<Character> sh4 = factory.makeCarrier(c2dPlacement);
    Ship<Character> sh5 = factory.makeCarrier(b5lPlacement);
    b1.tryAddShip(sh3);
    b1.tryAddShip(sh2);
    b1.tryAddShip(sh4);
    b1.tryAddShip(sh5);
    Character[][] expected = new Character[10][10];

    expected[0][0] = 'c';
    expected[1][0] = 'c';
    expected[2][0] = 'c';
    expected[3][0] = 'c';
    expected[2][1] = 'c';
    expected[3][1] = 'c';
    expected[4][1] = 'c';
    expected[1][2] = 'c';
    expected[2][2] = 'c';
    expected[3][2] = 'c';
    expected[4][2] = 'c';
    expected[0][3] = 'c';
    expected[1][3] = 'c';
    expected[2][3] = 'c';
    expected[3][3] = 'c';
    expected[4][3] = 'c';
    expected[5][3] = 'c';
    expected[6][3] = 'c';
    expected[0][4] = 'c';
    expected[1][4] = 'c';
    expected[0][5] = 'c';
    expected[2][5] = 'c';
    expected[0][6] = 'c';
    expected[2][6] = 'c';
    expected[1][7] = 'c';
    expected[2][7] = 'c';
    expected[1][8] = 'c';
    expected[2][8] = 'c';
    expected[1][9] = 'c';
    checkWhatIsAtBoard(b1, expected);
    // checkWhatIsAtBoard(b1, expected);
    // b1.makeMove(sh2, B0LPlacement);
    // checkWhatIsAtBoard(b1, expected);
    // b1.makeMove(sh2, B0RPlacement);
    // checkWhatIsAtBoard(b1, expected);
    // b1.makeMove(sh2, invalidPlacement);
    // checkWhatIsAtBoard(b1, expected);
  }

  @Test
  public void check_make_move_battleship() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 10, 'X');
    V2ShipFactory factory = new V2ShipFactory();
    // Placement invalidPlacement = new Placement("a0u");
    Placement A0UPlacement = new Placement("a0u");
    Placement B0LPlacement = new Placement("b0l");
    Placement A1LPlacement = new Placement("a1r");
    Placement A1DPlacement = new Placement("a1d");

    Placement invalidPlacement = new Placement("a9u");
    // Ship<Character> sh1 = factory.makeCarrier(invalidPlacement);
    // assertThrows(IllegalArgumentException.class, () -> b1.tryAddShip(sh1));
    Ship<Character> sh2 = factory.makeBattleship(A0UPlacement);
    b1.tryAddShip(sh2);
    Character[][] expected = new Character[10][10];
    // checkWhatIsAtBoard(b1, expected);
    // b1.makeMove(sh2, B0LPlacement);
    // checkWhatIsAtBoard(b1, expected);
    // b1.makeMove(sh2, A1LPlacement);
    // checkWhatIsAtBoard(b1, expected);
    // b1.makeMove(sh2, A1DPlacement);
    // checkWhatIsAtBoard(b1, expected);
  }

  @Test
  public void check_make_move_submarine() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 10, 'X');
    V2ShipFactory factory = new V2ShipFactory();
    // Placement invalidPlacement = new Placement("a0u");
    Placement A0VPlacement = new Placement("a0v");
    Placement B0HPlacement = new Placement("b0h");

    Placement invalidPlacement = new Placement("a9u");
    Ship<Character> sh1 = factory.makeCarrier(invalidPlacement);
    System.out.println(b1.tryAddShip(sh1));
    // assertThrows(IllegalArgumentException.class, () -> b1.tryAddShip(sh1));
    Ship<Character> sh2 = factory.makeSubmarine(A0VPlacement);
    b1.tryAddShip(sh2);
    Character[][] expected = new Character[10][10];
    // checkWhatIsAtBoard(b1, expected);
    b1.makeMove(sh2, B0HPlacement);
    Ship<Character> sh3 = factory.makeSubmarine(new Placement("A9h"));
    b1.tryAddShip(sh3);
    // checkWhatIsAtBoard(b1, expected);
  }

  @Test
  public void check_make_destroyer() {
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 10, 'X');
    V2ShipFactory factory = new V2ShipFactory();
    // Placement invalidPlacement = new Placement("a0u");
    Placement A0VPlacement = new Placement("a0v");
    Placement B0HPlacement = new Placement("b0h");

    Placement invalidPlacement = new Placement("a9u");
    // Ship<Character> sh1 = factory.makeCarrier(invalidPlacement);
    // assertThrows(IllegalArgumentException.class, () -> b1.tryAddShip(sh1));
    Ship<Character> sh2 = factory.makeDestroyer(A0VPlacement);
    b1.tryAddShip(sh2);
    Character[][] expected = new Character[10][10];
    // checkWhatIsAtBoard(b1, expected);
    b1.makeMove(sh2, B0HPlacement);
    // checkWhatIsAtBoard(b1, expected);
  }

  @Test
  public void test_invalid_constructor() {
    assertThrows(IllegalArgumentException.class, () -> new BattleShipBoard<Character>(-1, 10, 'x'));
    // assertEquals("BattleShipBoard's width must be positive but is -1", new
    // BattleShipBoard<Character>(-1, 10, 'x'));
  }

  @Test
  public void test_what_is_at() {
    BattleShipBoard<Character> b = new BattleShipBoard<Character>(10, 10, 'X');
    assertThrows(IllegalArgumentException.class, () -> b.whatIsAt(new Coordinate(12, 12), false));
  }

  // BattleShipBoardTest
  private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter,
      Coordinate... expectedLocs) {
    assertEquals(expectedName, testShip.getName());
    for (Coordinate coordinate : expectedLocs) {
      assertEquals(true, testShip.occupiesCoordinates(coordinate));
      assertEquals(expectedLetter, testShip.getDisplayInfoAt(coordinate, true));
    }

  }

  @Test
  public void test_carrier_ship() {
    V2ShipFactory factory = new V2ShipFactory();
    Ship<Character> sh = factory.makeBattleship(new Placement(new Coordinate("A1"), 'R'));
    Ship<Character> sh1 = factory.makeBattleship(new Placement(new Coordinate("A1"), 'L'));
    Ship<Character> sh2 = factory.makeBattleship(new Placement(new Coordinate("A1"), 'U'));
    Ship<Character> sh3 = factory.makeBattleship(new Placement(new Coordinate("A1"), 'D'));
    checkShip(sh, "Battleship", 'b', new Coordinate(0, 1), new Coordinate(1, 1), new Coordinate(2, 1),
        new Coordinate(1, 2));
    checkShip(sh1, "Battleship", 'b', new Coordinate(0, 2), new Coordinate(1, 2), new Coordinate(2, 2),
        new Coordinate(1, 1));
    checkShip(sh2, "Battleship", 'b', new Coordinate(0, 2), new Coordinate(1, 1), new Coordinate(1, 2),
        new Coordinate(1, 3));
    checkShip(sh3, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(0, 1), new Coordinate(0, 2),
        new Coordinate(0, 3));
  }

  // BoardTextViewTest
  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody) {
    Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'x');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  private void placementBoardHelper(BoardTextView v, String expectedHeader, String expectedBody) {
    assertEquals(expectedHeader, v.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, v.displayMyOwnBoard());
  }

  @Test
  public void test_display_empty_2x2() {
    String expectedHeader = "  0|1\n";
    String expected = "A  |  A\n" + "B  |  B\n";
    emptyBoardHelper(2, 2, expectedHeader, expected);
  }

  @Test
  public void test_display_empty_3x2() {
    String expectedHeader = "  0|1|2\n";
    String expected = "A  | |  A\n" + "B  | |  B\n";
    emptyBoardHelper(3, 2, expectedHeader, expected);
  }

  @Test
  public void test_display_empty_3x5() {
    String expectedHeader = "  0|1|2\n";
    String expected = "A  | |  A\n" + "B  | |  B\n" + "C  | |  C\n" + "D  | |  D\n" + "E  | |  E\n";
    emptyBoardHelper(3, 5, expectedHeader, expected);
  }

  @Test
  public void test_display_PlaceShip_3x5() {
    String expectedHeader = "  0|1|2\n";
    String expected = "A  | |  A\n" + "B s| |  B\n" + "C  |s|  C\n" + "D  | |  D\n" + "E  | |  E\n";

    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(3, 5, 'x');

    RectangleShip<Character> sh1 = new RectangleShip<Character>(new Coordinate(1, 0), 's', '*');
    RectangleShip<Character> sh2 = new RectangleShip<Character>(new Coordinate(2, 1), 's', '*');
    b1.tryAddShip(sh1);
    b1.tryAddShip(sh2);

    BoardTextView view = new BoardTextView(b1);
    placementBoardHelper(view, expectedHeader, expected);
  }

  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11, 20, 'x');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10, 27, 'x');
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
  }

  @Test
  public void test_display_enemy_3by2() {
    String myView = "  0|1|2|3\n" +
        "A  | | |d A\n" +
        "B s|*| |d B\n" +
        "C  | | |d C\n" +
        "  0|1|2|3\n";
    String enemyView = "  0|1|2|3\n" +
        "A  | | |  A\n" +
        "B  |s| |  B\n" +
        "C  |X| |  C\n" +
        "  0|1|2|3\n";
    Board<Character> b = new BattleShipBoard<Character>(4, 3, 'X');

    Placement p1 = new Placement("B0H");
    Placement p2 = new Placement("A3V");
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> s = f.makeSubmarine(p1);
    Ship<Character> d = f.makeDestroyer(p2);
    b.tryAddShip(s);
    b.tryAddShip(d);
    BoardTextView view = new BoardTextView(b);
    Coordinate fire = new Coordinate(1, 1);
    Coordinate miss = new Coordinate(2, 1);
    b.fireAt(fire);
    b.fireAt(miss);
    assertEquals(myView, view.displayMyOwnBoard());
    assertEquals(enemyView, view.displayEnemyBoard());

  }

  // @Test
  // public void test_display_my_board_with_enemy_next_to_it() {
  // Board<Character> b = new BattleShipBoard<Character>(10, 20, 'x');
  // BoardTextView enemyView = new BoardTextView(b);
  // BoardTextView view = new BoardTextView(b);
  // assertEquals("expected", view.displayMyBoardWithEnemyNextToIt(enemyView,
  // "Your ocean", "Player B's ocean"));
  // }

  @Test
  void test_display_two_boards() {
    Board<Character> b1 = new BattleShipBoard<Character>(4, 3, 'X');
    Board<Character> b2 = new BattleShipBoard<Character>(4, 3, 'X');
    BoardTextView v1 = new BoardTextView(b1);
    BoardTextView v2 = new BoardTextView(b2);
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> d = f.makeDestroyer(new Placement("A3V"));
    Ship<Character> s = f.makeDestroyer(new Placement("B0h"));
    b1.tryAddShip(d);
    b2.tryAddShip(s);

    String expected = "" +
        "     my board                   enemy board\n" +
        "  0|1|2|3                    0|1|2|3\n" +
        "A  | | |d A                A  | | |  A\n" +
        "B  | | |d B                B  | | |  B\n" +
        "C  | | |d C                C  | | |  C\n" +
        "  0|1|2|3                    0|1|2|3\n";
    assertEquals(expected, v1.displayMyBoardWithEnemyNextToIt(v2, "my board", "enemy board"));
  }

  // CarrierShipTest
  private void checkShip1(Ship<Character> testShip, String expectedName, char expectedLetter,
      Coordinate... expectedLocs) {
    assertEquals(expectedName, testShip.getName());
    for (Coordinate coordinate : expectedLocs) {
      assertEquals(true, testShip.occupiesCoordinates(coordinate));
      assertEquals(expectedLetter, testShip.getDisplayInfoAt(coordinate, true));
    }

  }

  @Test
  public void test_carrier_ship1() {
    V2ShipFactory factory = new V2ShipFactory();
    Ship<Character> sh = factory.makeCarrier(new Placement(new Coordinate("B0"), 'R'));
    Ship<Character> sh1 = factory.makeCarrier(new Placement(new Coordinate("B0"), 'L'));
    Ship<Character> sh2 = factory.makeCarrier(new Placement(new Coordinate("A0"), 'U'));
    Ship<Character> sh3 = factory.makeCarrier(new Placement(new Coordinate("A0"), 'D'));

    
    checkShip1(sh, "Carrier", 'c', new Coordinate(1, 1), new Coordinate(1, 2), new Coordinate(1, 3),
        new Coordinate(1, 4), new Coordinate(2, 0), new Coordinate(2, 1), new Coordinate(2, 1));
    checkShip1(sh1, "Carrier", 'c', new Coordinate(2, 0), new Coordinate(2, 1), new Coordinate(2, 2),
        new Coordinate(2, 3), new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));
    checkShip1(sh2, "Carrier", 'c', new Coordinate(0, 0), new Coordinate(1, 0), new Coordinate(2, 0),
        new Coordinate(3, 0), new Coordinate(2, 1), new Coordinate(3, 1), new Coordinate(4, 1));
    checkShip1(sh3, "Carrier", 'c', new Coordinate(0, 0), new Coordinate(1, 0), new Coordinate(2, 0),
        new Coordinate(1, 1), new Coordinate(2, 1), new Coordinate(3, 1), new Coordinate(4, 1));
  }

  // CoordinateTest
  @Test
  public void test_coordinates() {
    Coordinate c1 = new Coordinate(1, 2);
    assertEquals(c1.getRow(), 1);
    assertEquals(c1.getColumn(), 2);
  }

  @Test
  public void test_equals() {
    Coordinate c1 = new Coordinate(1, 2);
    Coordinate c2 = new Coordinate(1, 2);
    Coordinate c3 = new Coordinate(1, 3);
    Coordinate c4 = new Coordinate(3, 2);
    assertEquals(c1, c1);
    assertEquals(c1, c2);
    assertNotEquals(c1, c3);
    assertNotEquals(c1, c4);
    assertNotEquals(c3, c4);
    assertNotEquals(c1, "(1, 2)");
  }

  @Test
  public void test_hashCode() {
    Coordinate c1 = new Coordinate(1, 2);
    Coordinate c2 = new Coordinate(1, 2);
    Coordinate c3 = new Coordinate(1, 3);
    Coordinate c4 = new Coordinate(3, 2);
    assertEquals(c1.hashCode(), c2.hashCode());
    assertNotEquals(c1.hashCode(), c3.hashCode());
    assertNotEquals(c1.hashCode(), c4.hashCode());
  }

  @Test
  void test_string_constructor_valid_cases() {
    Coordinate c1 = new Coordinate("B3");
    assertEquals(1, c1.getRow());
    assertEquals(3, c1.getColumn());
    Coordinate c2 = new Coordinate("D5");
    assertEquals(3, c2.getRow());
    assertEquals(5, c2.getColumn());
    Coordinate c3 = new Coordinate("A9");
    assertEquals(0, c3.getRow());
    assertEquals(9, c3.getColumn());
    Coordinate c4 = new Coordinate("Z0");
    assertEquals(25, c4.getRow());
    assertEquals(0, c4.getColumn());
    Coordinate c5 = new Coordinate("z0");
    assertEquals(25, c5.getRow());
    assertEquals(0, c5.getColumn());

  }

  @Test
  public void test_string_constructor_error_cases() {
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("00"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("AA"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("@0"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("[0"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("A/"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("A:"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("A"));
    assertThrows(IllegalArgumentException.class, () -> new Coordinate("A12"));
  }

  // InBoundsRuleCheckerTest
  @Test
  void test_bound_rule_checker() {
    V1ShipFactory v1ShipFactory = new V1ShipFactory();
    InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'x');

    /* destroyer 1 */
    Placement v1_1 = new Placement("a0v");
    Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);

    /* destroyer 2 */
    Placement v1_2 = new Placement(new Coordinate(-1, 2), 'V');
    Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v1_2);

    /* destroyer 3 */
    Placement v1_3 = new Placement(new Coordinate(1, -1), 'V');
    Ship<Character> dst3 = v1ShipFactory.makeDestroyer(v1_3);

    /* destroyer 4 */
    Placement v1_4 = new Placement(new Coordinate(10, 2), 'H');
    Ship<Character> dst4 = v1ShipFactory.makeDestroyer(v1_4);

    /* destroyer 4 */
    Placement v1_5 = new Placement(new Coordinate(10, 20), 'H');
    Ship<Character> dst5 = v1ShipFactory.makeDestroyer(v1_5);

    // /* destroyer 6 */
    Placement v1_6 = new Placement("T0V");
    Ship<Character> dst6 = v1ShipFactory.makeDestroyer(v1_6);

    assertEquals(null, checker.checkMyRule(dst1, b1));
    assertEquals("That placement is invalid: the ship goes off the top of the board.", checker.checkMyRule(dst2, b1));
    assertEquals("That placement is invalid: the ship goes off the left of the board.", checker.checkMyRule(dst3, b1));
    assertEquals(null, checker.checkMyRule(dst4, b1));
    assertEquals("That placement is invalid: the ship goes off the right of the board.", checker.checkMyRule(dst5, b1));
    assertEquals("That placement is invalid: the ship goes off the bottom of the board.",
        checker.checkMyRule(dst6, b1));

  }

  // NoCollisionRuleCheckerTest
  @Test
  void test_bound_collisionrule_checker() {
    V2ShipFactory v1ShipFactory = new V2ShipFactory();
    NoCollisionRuleChecker<Character> checker = new NoCollisionRuleChecker<>(null);
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'x');

    /* destroyer 1 */
    Placement v1_1 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);
    b1.tryAddShip(dst1);

    /* destroyer 2 */
    Placement v1_2 = new Placement(new Coordinate(2, 3), 'V');
    Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v1_2);
    b1.tryAddShip(dst2);
    assertEquals("That placement is invalid: the ship overlaps another ship.", checker.checkMyRule(dst2, b1));

    /* destroyer 4 - test collision */
    Placement v1_4 = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> dst4 = v1ShipFactory.makeDestroyer(v1_4);
    assertEquals("That placement is invalid: the ship overlaps another ship.", b1.tryAddShip(dst4));

    /* destroyer 5 - test collision */
    Placement v1_5 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst5 = v1ShipFactory.makeDestroyer(v1_5);
    assertEquals("That placement is invalid: the ship overlaps another ship.", checker.checkMyRule(dst5, b1));

    Placement v1_6 = new Placement(new Coordinate(2, 2), 'H');
    Ship<Character> dst6 = v1ShipFactory.makeDestroyer(v1_6);
    assertEquals("That placement is invalid: the ship overlaps another ship.", checker.checkMyRule(dst6, b1));

  }

  // PlacementRuleCheckerTest
  @Test
  void test_combine_rules() {
    V1ShipFactory v1ShipFactory = new V1ShipFactory();
    NoCollisionRuleChecker<Character> checker = new NoCollisionRuleChecker<>(new InBoundsRuleChecker<Character>(null));
    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'x');

    /* destroyer 1 */
    Placement v1_1 = new Placement(new Coordinate(0, 0), 'V');
    Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);
    b1.tryAddShip(dst1);

    /* destroyer 2 */
    Placement v1_2 = new Placement(new Coordinate(0, 1), 'V');
    Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v1_2);
    b1.tryAddShip(dst2);

    /* destroyer 3 - cannot add */
    Placement v1_3 = new Placement(new Coordinate(0, 1), 'V');
    Ship<Character> dst3 = v1ShipFactory.makeDestroyer(v1_3);
    assertEquals("That placement is invalid: the ship overlaps another ship.", b1.tryAddShip(dst3));

    /* destroyer 4 - test bound */
    Placement v1_4 = new Placement(new Coordinate(-1, 1), 'H');
    Ship<Character> dst4 = v1ShipFactory.makeDestroyer(v1_4);
    assertEquals("That placement is invalid: the ship goes off the top of the board.",
        checker.checkPlacement(dst4, b1));

    /* destroyer 5 - test collision */
    Placement v1_5 = new Placement(new Coordinate(19, 0), 'V');
    Ship<Character> dst5 = v1ShipFactory.makeDestroyer(v1_5);
    // assertEquals("That placement is invalid: the ship goes off the bottom of the
    // board.", checker.checkPlacement(dst5,b1));

    /* destroyer 6 - test normal */
    Placement v1_6 = new Placement(new Coordinate(10, 9), 'H');
    Ship<Character> dst6 = v1ShipFactory.makeDestroyer(v1_6);
    assertThrows(IllegalArgumentException.class, () -> checker.checkPlacement(dst6, b1));

  }

  // PlacementTest
  @Test
  public void test_constructor_invalid() {
    assertThrows(IllegalArgumentException.class, () -> new Placement(new Coordinate("A9"), 'o'));
  }

  @Test
  public void test_equal() {
    Placement p1 = new Placement("A0H");
    Placement p2 = new Placement("A0H");
    Placement p3 = new Placement("A0h");
    Placement p4 = new Placement("A1H");
    Placement p5 = new Placement("B0H");
    Placement p6 = new Placement(new Coordinate("A0"), 'V');
    Placement p7 = new Placement(new Coordinate("A1"), 'h');

    Coordinate c1 = new Coordinate("B0");
    assertEquals(p1, p1); // equals should be reflexsive
    assertEquals(p1, p2); // different objects bu same contents
    assertEquals(p1, p3); // different cases
    assertNotEquals(p1, p4);
    assertNotEquals(p1, p5);
    assertTrue(p1.equals(p2));
    assertFalse(p1.equals(p4));
    assertFalse(p7.equals(p6));
    assertEquals(p5, new Placement(c1, 'H'));
    assertNotEquals(p1, "(0, 0, H)"); // different types
  }

  @Test
  public void test_hashCode2() {
    Placement c1 = new Placement("A0H");
    Placement c2 = new Placement("A0h");
    Placement c3 = new Placement("A1H");
    Placement c4 = new Placement("B0H");
    assertEquals(c1.hashCode(), c2.hashCode());
    assertNotEquals(c1.hashCode(), c3.hashCode());
    assertNotEquals(c1.hashCode(), c4.hashCode());
  }

  @Test
  void test_string_constructor_valid_cases2() {
    Placement p1 = new Placement("A0H");
    assertEquals('H', p1.getOrientation());
    Coordinate c1 = new Coordinate("A0");
    assertEquals(p1.getWhere(), c1);

    Placement p2 = new Placement("D5H");
    assertEquals('H', p2.getOrientation());
    Coordinate c2 = new Coordinate("D5");
    assertEquals(p2.getWhere(), c2);

    Placement p3 = new Placement("A9V");
    assertEquals('V', p3.getOrientation());
    Coordinate c3 = new Coordinate("A9");
    assertEquals(p3.getWhere(), c3);

    Placement p4 = new Placement("Z0V");
    assertEquals('V', p4.getOrientation());
    Coordinate c4 = new Coordinate("Z0");
    assertEquals(p4.getWhere(), c4);
  }

  @Test
  public void test_string_constructor_error_cases2() {
    assertThrows(IllegalArgumentException.class, () -> new Placement("00H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("AAH"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("@0V"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("[0H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A/V"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A:H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A0"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A12"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A9a"));
    Coordinate c1 = new Coordinate("B0");
    assertThrows(IllegalArgumentException.class, () -> new Placement(c1, 'a'));
  }

  // RectangleShipTest
  @Test
  public void test_rectangleship_constrct() {
    Coordinate c = new Coordinate(5, 6);
    Coordinate c1 = new Coordinate(6, 7);
    Coordinate c2 = new Coordinate(7, 9);
    ShipDisplayInfo<Character> i = new SimpleShipDisplayInfo<Character>('t', 'a');
    ShipDisplayInfo<Character> i1 = new SimpleShipDisplayInfo<Character>('e', 'x');

    RectangleShip<Character> s1 = new RectangleShip<Character>("submarine", c, 2, 3, 's', '*');
    assertEquals(true, s1.occupiesCoordinates(c));
    assertEquals(6, s1.myPieces.size());

    RectangleShip<Character> s2 = new RectangleShip<Character>("submarine", c, 2, 3, i, i1);
    assertEquals(true, s2.occupiesCoordinates(c1));
    assertEquals(false, s2.occupiesCoordinates(c2));

    RectangleShip<Character> s3 = new RectangleShip<Character>("submarine", c, 2, 3, 's', '*');
    assertEquals(true, s3.occupiesCoordinates(c1));
    assertEquals(false, s3.occupiesCoordinates(c2));

    List<Coordinate> c3 = new ArrayList<Coordinate>();
    c3.add(c);
    c3.add(c1);
    c3.add(c2);
    assertEquals(false, s3.occupiesCoordinates(c3));

  }

  @Test
  public void test_coordinate_not_in_ship() {
    Coordinate c1 = new Coordinate(8, 4);
    Coordinate c2 = new Coordinate(7, 5);
    RectangleShip<Character> s = new RectangleShip<Character>("submarine", c1, 2, 3, 's', '*');
    assertThrows(IllegalArgumentException.class, () -> s.checkCoordinateInThisShip(c2));
  }

  @Test
  public void test_hit() {
    Coordinate c1 = new Coordinate(8, 4);
    Coordinate c2 = new Coordinate(9, 5);
    RectangleShip<Character> s = new RectangleShip<Character>("submarine", c1, 2, 3, 's', '*');
    s.recordHitAt(c2);
    assertEquals(true, s.wasHitAt(c2));
    assertNotEquals(true, s.wasHitAt(c1));
  }

  @Test
  public void test_sunk() {
    Coordinate c1 = new Coordinate(2, 3);
    Coordinate c2 = new Coordinate(2, 4);
    RectangleShip<Character> s = new RectangleShip<Character>("submarine", c1, 2, 1, 's', '*');
    s.recordHitAt(c2);
    assertEquals(false, s.isSunk());
    s.recordHitAt(c1);
    assertNotEquals(false, s.isSunk());
  }

  @Test
  public void test_occupiesCoordinates() {
    HashSet<Coordinate> w1 = new HashSet<Coordinate>();
    w1.add(new Coordinate(5, 5));
    w1.add(new Coordinate(5, 6));
    w1.add(new Coordinate(6, 5));
    w1.add(new Coordinate(6, 6));
    HashSet<Coordinate> w2 = new HashSet<Coordinate>();
    w2.add(new Coordinate(5, 5));
    w2.add(new Coordinate(5, 6));
    HashSet<Coordinate> w3 = new HashSet<Coordinate>();
    w3.add(new Coordinate(5, 5));
    w3.add(new Coordinate(5, 9));
    RectangleShip<Character> s = new RectangleShip<Character>("Submarine", new Coordinate(5, 5), 2, 2, 's', '*');
    assertEquals(true, s.occupiesCoordinates(w1));
    assertEquals(true, s.occupiesCoordinates(w2));
    assertNotEquals(true, s.occupiesCoordinates(w3));

    assertEquals("Submarine", s.getName());
  }

  // TextPlayerTest
  private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
    BufferedReader input = new BufferedReader(new StringReader(inputData));
    PrintStream output = new PrintStream(bytes, true);
    Board<Character> board = new BattleShipBoard<Character>(w, h, 'x');
    V1ShipFactory shipFactory = new V1ShipFactory();
    return new TextPlayer("A", board, input, output, shipFactory);
  }

  @Test
  void test_read_placement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

    String prompt = "Please enter a location for a ship:";
    Placement[] expected = new Placement[3];
    expected[0] = new Placement(new Coordinate(1, 2), 'V');
    expected[1] = new Placement(new Coordinate(2, 8), 'H');
    expected[2] = new Placement(new Coordinate(0, 4), 'V');

    for (int i = 0; i < expected.length; i++) {
      Placement p = player.readPlacement(prompt);
      assertEquals(p, expected[i]); // did we get the right Placement back
      // System.out.println(bytes.toString());
      // assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt
      // and newline
      bytes.reset(); // clear out bytes for next time around
    }
  }

  @Test
  void test_doOnePlacement() throws IOException {
    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    TextPlayer player = createTextPlayer(4, 4, "B1V\n", bytes);

    String expectedHeader = "  0|1|2|3\n";
    String expectedBody = "A  | | |  A\n" +
        "B  |d| |  B\n" +
        "C  |d| |  C\n" +
        "D  |d| |  D\n";

    String expected = "\nPlayer A where do you want to place a Destroyer?\n\nCurrent ocean:\n" + expectedHeader
        + expectedBody + expectedHeader + "\n";

    V1ShipFactory shipFactory = new V1ShipFactory();
    // player.doOnePlacement("Destroyer", (p)->shipFactory.makeDestroyer(p));
    player.doOnePlacement("Destroyer", (p) -> shipFactory.makeDestroyer(p));
    System.out.print(bytes.toString());

    assertEquals(expected, bytes.toString());
  }






}