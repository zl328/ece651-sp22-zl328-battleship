package edu.duke.zl328.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class BattleShipTest {
    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate coordinate : expectedLocs) {
            assertEquals(true, testShip.occupiesCoordinates(coordinate));
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(coordinate, true));                   
        }
        
    }

    @Test
    public void test_carrier_ship() {
        V2ShipFactory factory = new V2ShipFactory();
        Ship<Character> sh = factory.makeBattleship(new Placement(new Coordinate("A1"), 'R'));
        Ship<Character> sh1 = factory.makeBattleship(new Placement(new Coordinate("A1"), 'L'));
        Ship<Character> sh2 = factory.makeBattleship(new Placement(new Coordinate("A1"), 'U'));
        Ship<Character> sh3 = factory.makeBattleship(new Placement(new Coordinate("A1"), 'D'));
        checkShip(sh, "Battleship", 'b', new Coordinate(0,1), new Coordinate(1,1), new Coordinate(2,1), new Coordinate(1, 2));
        checkShip(sh1, "Battleship", 'b', new Coordinate(0,2), new Coordinate(1,2), new Coordinate(2,2), new Coordinate(1,1));
        checkShip(sh2, "Battleship", 'b', new Coordinate(0,2), new Coordinate(1,1), new Coordinate(1,2), new Coordinate(1,3));
        checkShip(sh3, "Battleship", 'b', new Coordinate(1,2), new Coordinate(0,1), new Coordinate(0,2), new Coordinate(0,3));
    }
}
