
package edu.duke.zl328.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class BoardTextViewTest {
  private void emptyBoardHelper(int w, int h, String expectedHeader, String expectedBody) {
    Board<Character> b1 = new BattleShipBoard<Character>(w, h, 'x');
    BoardTextView view = new BoardTextView(b1);
    assertEquals(expectedHeader, view.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, view.displayMyOwnBoard());
  }

  private void placementBoardHelper(BoardTextView v, String expectedHeader, String expectedBody) {
    assertEquals(expectedHeader, v.makeHeader());
    String expected = expectedHeader + expectedBody + expectedHeader;
    assertEquals(expected, v.displayMyOwnBoard());
  }

  @Test
  public void test_display_empty_2x2() {
    String expectedHeader = "  0|1\n";
    String expected = "A  |  A\n" + "B  |  B\n";
    emptyBoardHelper(2, 2, expectedHeader, expected);
  }

  @Test
  public void test_display_empty_3x2() {
    String expectedHeader = "  0|1|2\n";
    String expected = "A  | |  A\n" + "B  | |  B\n";
    emptyBoardHelper(3, 2, expectedHeader, expected);
  }

  @Test
  public void test_display_empty_3x5() {
    String expectedHeader = "  0|1|2\n";
    String expected = "A  | |  A\n" + "B  | |  B\n" + "C  | |  C\n" + "D  | |  D\n" + "E  | |  E\n";
    emptyBoardHelper(3, 5, expectedHeader, expected);
  }

  @Test
  public void test_display_PlaceShip_3x5() {
    String expectedHeader = "  0|1|2\n";
    String expected = "A  | |  A\n" + "B s| |  B\n" + "C  |s|  C\n" + "D  | |  D\n" + "E  | |  E\n";

    BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(3, 5, 'x');

    RectangleShip<Character> sh1 = new RectangleShip<Character>(new Coordinate(1, 0), 's', '*');
    RectangleShip<Character> sh2 = new RectangleShip<Character>(new Coordinate(2, 1), 's', '*');
    b1.tryAddShip(sh1);
    b1.tryAddShip(sh2);

    BoardTextView view = new BoardTextView(b1);
    placementBoardHelper(view, expectedHeader, expected);
  }

  @Test
  public void test_invalid_board_size() {
    Board<Character> wideBoard = new BattleShipBoard<Character>(11, 20, 'x');
    Board<Character> tallBoard = new BattleShipBoard<Character>(10, 27, 'x');
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(wideBoard));
    assertThrows(IllegalArgumentException.class, () -> new BoardTextView(tallBoard));
  }

  @Test
  public void test_display_enemy_3by2() {
    String myView = 
        "  0|1|2|3\n" +
        "A  | | |d A\n" +
        "B s|*| |d B\n" +
        "C  | | |d C\n" +
        "  0|1|2|3\n";
    String enemyView = 
        "  0|1|2|3\n" +
        "A  | | |  A\n" +
        "B  |s| |  B\n" +
        "C  |X| |  C\n" +
        "  0|1|2|3\n";
    Board<Character> b = new BattleShipBoard<Character>(4, 3, 'X');

    Placement p1 = new Placement("B0H");
    Placement p2 = new Placement("A3V");
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> s = f.makeSubmarine(p1);
    Ship<Character> d = f.makeDestroyer(p2);
    b.tryAddShip(s);
    b.tryAddShip(d);
    BoardTextView view = new BoardTextView(b);
    Coordinate fire = new Coordinate(1, 1);
    Coordinate miss = new Coordinate(2, 1);
    b.fireAt(fire);
    b.fireAt(miss);
    assertEquals(myView, view.displayMyOwnBoard());
    assertEquals(enemyView, view.displayEnemyBoard());

  }

  // @Test
  // public void test_display_my_board_with_enemy_next_to_it() {
  // Board<Character> b = new BattleShipBoard<Character>(10, 20, 'x');
  // BoardTextView enemyView = new BoardTextView(b);
  // BoardTextView view = new BoardTextView(b);
  // assertEquals("expected", view.displayMyBoardWithEnemyNextToIt(enemyView,
  // "Your ocean", "Player B's ocean"));
  // }

  @Test
  void test_display_two_boards(){
    Board<Character> b1 = new BattleShipBoard<Character>(4,3,'X');
    Board<Character> b2 = new BattleShipBoard<Character>(4,3,'X');
    BoardTextView v1 = new BoardTextView(b1);
    BoardTextView v2 = new BoardTextView(b2);
    V1ShipFactory f = new V1ShipFactory();
    Ship<Character> d = f.makeDestroyer(new Placement("A3V"));
    Ship<Character> s = f.makeDestroyer(new Placement("B0h"));
    b1.tryAddShip(d);
    b2.tryAddShip(s);

    String expected =""+
      "     my board                   enemy board\n"+
      "  0|1|2|3                    0|1|2|3\n"+
      "A  | | |d A                A  | | |  A\n"+
      "B  | | |d B                B  | | |  B\n"+
      "C  | | |d C                C  | | |  C\n"+
      "  0|1|2|3                    0|1|2|3\n";
    assertEquals(expected,v1.displayMyBoardWithEnemyNextToIt(v2, "my board", "enemy board"));
  }



}
