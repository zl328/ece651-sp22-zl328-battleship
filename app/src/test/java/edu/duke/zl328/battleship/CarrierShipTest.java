package edu.duke.zl328.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

public class CarrierShipTest {
    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate coordinate : expectedLocs) {
            assertEquals(true, testShip.occupiesCoordinates(coordinate));
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(coordinate, true));       
        }
        
    }

    @Test
    public void test_carrier_ship() {
        V2ShipFactory factory = new V2ShipFactory();
        Ship<Character> sh = factory.makeCarrier(new Placement(new Coordinate("B0"), 'R'));
        Ship<Character> sh1 = factory.makeCarrier(new Placement(new Coordinate("B0"), 'L'));
        Ship<Character> sh2 = factory.makeCarrier(new Placement(new Coordinate("A0"), 'U'));
        Ship<Character> sh3 = factory.makeCarrier(new Placement(new Coordinate("A0"), 'D'));
        checkShip(sh, "Carrier", 'c', new Coordinate(1,1), new Coordinate(1,2), new Coordinate(1,3), new Coordinate(1,4), new Coordinate(2,0), new Coordinate(2,1), new Coordinate(2,1));
        checkShip(sh1, "Carrier", 'c', new Coordinate(2,0), new Coordinate(2,1), new Coordinate(2,2), new Coordinate(2,3), new Coordinate(1,2), new Coordinate(1,3), new Coordinate(1,4));
        checkShip(sh2, "Carrier", 'c', new Coordinate(0,0), new Coordinate(1,0), new Coordinate(2,0), new Coordinate(3,0), new Coordinate(2,1), new Coordinate(3,1), new Coordinate(4,1));
        checkShip(sh3, "Carrier", 'c', new Coordinate(0,0), new Coordinate(1,0), new Coordinate(2,0), new Coordinate(1,1), new Coordinate(2,1), new Coordinate(3,1), new Coordinate(4,1));
    }
}
