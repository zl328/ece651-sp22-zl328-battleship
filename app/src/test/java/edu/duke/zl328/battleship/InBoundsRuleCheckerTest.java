package edu.duke.zl328.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class InBoundsRuleCheckerTest {
    @Test
    void test_bound_rule_checker(){
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        InBoundsRuleChecker<Character> checker = new InBoundsRuleChecker<Character>(null);
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'x');

        /* destroyer 1 */
        Placement v1_1 = new Placement("a0v");
        Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);

        /* destroyer 2 */
        Placement v1_2 = new Placement(new Coordinate(-1, 2), 'V');
        Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v1_2);

        /* destroyer 3 */
        Placement v1_3 = new Placement(new Coordinate(1, -1), 'V');
        Ship<Character> dst3 = v1ShipFactory.makeDestroyer(v1_3);

        /* destroyer 4 */
        Placement v1_4 = new Placement(new Coordinate(10, 2), 'H');
        Ship<Character> dst4 = v1ShipFactory.makeDestroyer(v1_4);

        /* destroyer 4 */
        Placement v1_5 = new Placement(new Coordinate(10, 20), 'H');
        Ship<Character> dst5 = v1ShipFactory.makeDestroyer(v1_5);

        // /* destroyer 6 */
        Placement v1_6 = new Placement("T0V");
        Ship<Character> dst6 = v1ShipFactory.makeDestroyer(v1_6);

        assertEquals(null, checker.checkMyRule(dst1,b1));
        assertEquals("That placement is invalid: the ship goes off the top of the board.", checker.checkMyRule(dst2,b1));
        assertEquals("That placement is invalid: the ship goes off the left of the board.", checker.checkMyRule(dst3,b1));
        assertEquals(null, checker.checkMyRule(dst4,b1));
        assertEquals("That placement is invalid: the ship goes off the right of the board.", checker.checkMyRule(dst5,b1));
        assertEquals("That placement is invalid: the ship goes off the bottom of the board.", checker.checkMyRule(dst6,b1));

    }

}
