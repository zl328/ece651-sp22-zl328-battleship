package edu.duke.zl328.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class NoCollisionRuleCheckerTest {
    @Test
    void test_bound_rule_checker(){
        V2ShipFactory v1ShipFactory = new V2ShipFactory();
        NoCollisionRuleChecker<Character> checker = new NoCollisionRuleChecker<>(null);
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'x');

        /* destroyer 1 */
        Placement v1_1 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);
        b1.tryAddShip(dst1);

        /* destroyer 2 */
        Placement v1_2 = new Placement(new Coordinate(2, 3), 'V');
        Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v1_2);
        b1.tryAddShip(dst2);
        assertEquals("That placement is invalid: the ship overlaps another ship.", checker.checkMyRule(dst2,b1));
        

        /* destroyer 4 - test collision */
        Placement v1_4 = new Placement(new Coordinate(1, 2), 'H');
        Ship<Character> dst4 = v1ShipFactory.makeDestroyer(v1_4);
        assertEquals("That placement is invalid: the ship overlaps another ship.", b1.tryAddShip(dst4));

        /* destroyer 5 - test collision */
        Placement v1_5 = new Placement(new Coordinate(1, 2), 'V');
        Ship<Character> dst5 = v1ShipFactory.makeDestroyer(v1_5);
        assertEquals("That placement is invalid: the ship overlaps another ship.", checker.checkMyRule(dst5,b1));

        Placement v1_6 = new Placement(new Coordinate(2, 2), 'H');
        Ship<Character> dst6 = v1ShipFactory.makeDestroyer(v1_6);
        assertEquals("That placement is invalid: the ship overlaps another ship.", checker.checkMyRule(dst6,b1));


    }
}