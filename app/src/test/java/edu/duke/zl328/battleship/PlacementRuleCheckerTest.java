package edu.duke.zl328.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class PlacementRuleCheckerTest {
    @Test
    void test_combine_rules(){
        V1ShipFactory v1ShipFactory = new V1ShipFactory();
        NoCollisionRuleChecker<Character> checker = new NoCollisionRuleChecker<>(new InBoundsRuleChecker<Character>(null));
        BattleShipBoard<Character> b1 = new BattleShipBoard<Character>(10, 20, 'x');

        /* destroyer 1 */
        Placement v1_1 = new Placement(new Coordinate(0, 0), 'V');
        Ship<Character> dst1 = v1ShipFactory.makeDestroyer(v1_1);
        b1.tryAddShip(dst1);

        /* destroyer 2 */
        Placement v1_2 = new Placement(new Coordinate(0, 1), 'V');
        Ship<Character> dst2 = v1ShipFactory.makeDestroyer(v1_2);
        b1.tryAddShip(dst2);

        /* destroyer 3 - cannot add */
        Placement v1_3 = new Placement(new Coordinate(0, 1), 'V');
        Ship<Character> dst3 = v1ShipFactory.makeDestroyer(v1_3);
        assertEquals("That placement is invalid: the ship overlaps another ship.", b1.tryAddShip(dst3));
        
        /* destroyer 4 - test bound */
        Placement v1_4 = new Placement(new Coordinate(-1, 1), 'H');
        Ship<Character> dst4 = v1ShipFactory.makeDestroyer(v1_4);
        assertEquals("That placement is invalid: the ship goes off the top of the board.", checker.checkPlacement(dst4,b1));

        /* destroyer 5 - test collision */
        Placement v1_5 = new Placement(new Coordinate(19, 0), 'V');
        Ship<Character> dst5 = v1ShipFactory.makeDestroyer(v1_5);
        // assertEquals("That placement is invalid: the ship goes off the bottom of the board.", checker.checkPlacement(dst5,b1));

        /* destroyer 6 - test normal */
        Placement v1_6 = new Placement(new Coordinate(10, 9), 'H');
        Ship<Character> dst6 = v1ShipFactory.makeDestroyer(v1_6);
        assertThrows(IllegalArgumentException.class, () -> checker.checkPlacement(dst6,b1));

    }


}
