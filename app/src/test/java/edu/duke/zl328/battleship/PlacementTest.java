package edu.duke.zl328.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

public class PlacementTest {

  @Test
  public void test_constructor_invalid() {
    assertThrows(IllegalArgumentException.class, () -> new Placement(new Coordinate("A9"), 'o'));
  }
  @Test
  public void test_equals() {
    Placement p1 = new Placement("A0H");
    Placement p2 = new Placement("A0H");
    Placement p3 = new Placement("A0h");
    Placement p4 = new Placement("A1H");
    Placement p5 = new Placement("B0H");
    Placement p6 = new Placement(new Coordinate("A0"), 'V');
    Placement p7 = new Placement(new Coordinate("A1"), 'h');

    Coordinate c1 = new Coordinate("B0");
    assertEquals(p1, p1); // equals should be reflexsive
    assertEquals(p1, p2); // different objects bu same contents
    assertEquals(p1, p3); // different cases
    assertNotEquals(p1, p4);
    assertNotEquals(p1, p5);
    assertTrue(p1.equals(p2));
    assertFalse(p1.equals(p4));
    assertFalse(p7.equals(p6));
    assertEquals(p5, new Placement(c1, 'H'));
    assertNotEquals(p1, "(0, 0, H)"); // different types
  }

  @Test
  public void test_hashCode() {
    Placement c1 = new Placement("A0H");
    Placement c2 = new Placement("A0h");
    Placement c3 = new Placement("A1H");
    Placement c4 = new Placement("B0H");
    assertEquals(c1.hashCode(), c2.hashCode());
    assertNotEquals(c1.hashCode(), c3.hashCode());
    assertNotEquals(c1.hashCode(), c4.hashCode());
  }

  @Test
  void test_string_constructor_valid_cases() {
    Placement p1 = new Placement("A0H");
    assertEquals('H', p1.getOrientation());
    Coordinate c1 = new Coordinate("A0");
    assertEquals(p1.getWhere(), c1);

    Placement p2 = new Placement("D5H");
    assertEquals('H', p2.getOrientation());
    Coordinate c2 = new Coordinate("D5");
    assertEquals(p2.getWhere(), c2);

    Placement p3 = new Placement("A9V");
    assertEquals('V', p3.getOrientation());
    Coordinate c3 = new Coordinate("A9");
    assertEquals(p3.getWhere(), c3);

    Placement p4 = new Placement("Z0V");
    assertEquals('V', p4.getOrientation());
    Coordinate c4 = new Coordinate("Z0");
    assertEquals(p4.getWhere(), c4);
  }

  @Test
  public void test_string_constructor_error_cases() {
    assertThrows(IllegalArgumentException.class, () -> new Placement("00H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("AAH"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("@0V"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("[0H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A/V"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A:H"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A0"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A12"));
    assertThrows(IllegalArgumentException.class, () -> new Placement("A9a"));
    Coordinate c1 = new Coordinate("B0");
    assertThrows(IllegalArgumentException.class, () -> new Placement(c1, 'a'));
  }

}
