package edu.duke.zl328.battleship;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;
import java.util.*;

public class RectangleShipTest {
    // @Test 
    // public void test_make_cords() {
    //     Coordinate c  = new Coordinate(5, 6);
    //     RectangleShip<Character> sh1 = new Ship<T>() {
            
    //     };
    // }


    @Test
    public void test_rectangleship_constrct() {
        Coordinate c = new Coordinate(5, 6);
        Coordinate c1 = new Coordinate(6, 7);
        Coordinate c2 = new Coordinate(7, 9);
        ShipDisplayInfo<Character> i = new SimpleShipDisplayInfo<Character>('t', 'a');
        ShipDisplayInfo<Character> i1 = new SimpleShipDisplayInfo<Character>('e', 'x');

        RectangleShip<Character> s1 = new RectangleShip<Character>("submarine", c, 2, 3, 's', '*');
        assertEquals(true, s1.occupiesCoordinates(c));
        assertEquals(6, s1.myPieces.size());

        RectangleShip<Character> s2 = new RectangleShip<Character>("submarine", c, 2, 3, i, i1);
        assertEquals(true, s2.occupiesCoordinates(c1));
        assertEquals(false, s2.occupiesCoordinates(c2));

        RectangleShip<Character> s3 = new RectangleShip<Character>("submarine", c, 2, 3, 's', '*');
        assertEquals(true, s3.occupiesCoordinates(c1));
        assertEquals(false, s3.occupiesCoordinates(c2));

        List<Coordinate> c3 = new ArrayList<Coordinate>();
        c3.add(c);
        c3.add(c1);
        c3.add(c2);
        assertEquals(false, s3.occupiesCoordinates(c3));

    }

    @Test
    public void test_coordinate_not_in_ship() {
        Coordinate c1 = new Coordinate(8, 4);
        Coordinate c2 = new Coordinate(7, 5);
        RectangleShip<Character> s = new RectangleShip<Character>("submarine", c1, 2, 3, 's', '*');
        assertThrows(IllegalArgumentException.class, () -> s.checkCoordinateInThisShip(c2));
    }

    @Test
    public void test_hit() {
        Coordinate c1 = new Coordinate(8, 4);
        Coordinate c2 = new Coordinate(9, 5);
        RectangleShip<Character> s = new RectangleShip<Character>("submarine", c1, 2, 3, 's', '*');
        s.recordHitAt(c2);
        assertEquals(true, s.wasHitAt(c2));
        assertNotEquals(true, s.wasHitAt(c1));
    }

    @Test
    public void test_sunk() {
        Coordinate c1 = new Coordinate(2, 3);
        Coordinate c2 = new Coordinate(2, 4);
        RectangleShip<Character> s = new RectangleShip<Character>("submarine", c1, 2, 1, 's', '*');
        s.recordHitAt(c2);
        assertEquals(false, s.isSunk());
        s.recordHitAt(c1);
        assertNotEquals(false, s.isSunk());
    }

    @Test
    public void test_occupiesCoordinates() {
        HashSet<Coordinate> w1 = new HashSet<Coordinate>();
        w1.add(new Coordinate(5, 5));
        w1.add(new Coordinate(5, 6));
        w1.add(new Coordinate(6, 5));
        w1.add(new Coordinate(6, 6));
        HashSet<Coordinate> w2 = new HashSet<Coordinate>();
        w2.add(new Coordinate(5, 5));
        w2.add(new Coordinate(5, 6));
        HashSet<Coordinate> w3 = new HashSet<Coordinate>();
        w3.add(new Coordinate(5, 5));
        w3.add(new Coordinate(5, 9));
        RectangleShip<Character> s = new RectangleShip<Character>("Submarine", new Coordinate(5, 5), 2, 2, 's', '*');
        assertEquals(true, s.occupiesCoordinates(w1));
        assertEquals(true, s.occupiesCoordinates(w2));
        assertNotEquals(true, s.occupiesCoordinates(w3));

        assertEquals("Submarine",s.getName());
    }
}
