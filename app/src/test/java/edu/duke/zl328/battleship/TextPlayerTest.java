package edu.duke.zl328.battleship;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import java.io.*;

public class TextPlayerTest {
    private TextPlayer createTextPlayer(int w, int h, String inputData, OutputStream bytes) {
        BufferedReader input = new BufferedReader(new StringReader(inputData));
        PrintStream output = new PrintStream(bytes, true);
        Board<Character> board = new BattleShipBoard<Character>(w, h, 'x');
        V1ShipFactory shipFactory = new V1ShipFactory();
        return new TextPlayer("A", board, input, output, shipFactory);
    }
    
    @Test
    void test_read_placement() throws IOException{
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        TextPlayer player = createTextPlayer(10, 20, "B2V\nC8H\na4v\n", bytes);

        String prompt = "Please enter a location for a ship:";
        Placement[] expected = new Placement[3];
        expected[0] = new Placement(new Coordinate(1, 2), 'V');
        expected[1] = new Placement(new Coordinate(2, 8), 'H');
        expected[2] = new Placement(new Coordinate(0, 4), 'V');

        for (int i = 0; i < expected.length; i++) {
            Placement p = player.readPlacement(prompt);
            assertEquals(p, expected[i]); //did we get the right Placement back
            // System.out.println(bytes.toString());
            //assertEquals(prompt + "\n", bytes.toString()); //should have printed prompt and newline
            bytes.reset(); //clear out bytes for next time around
        }
    }

    @Test
    void test_doOnePlacement() throws IOException{
      ByteArrayOutputStream bytes = new ByteArrayOutputStream();
      TextPlayer player = createTextPlayer(4, 4, "B1V\n", bytes);
      
      String expectedHeader = "  0|1|2|3\n";
      String expectedBody =
        "A  | | |  A\n"+
        "B  |d| |  B\n"+
        "C  |d| |  C\n"+
        "D  |d| |  D\n";
        
      String expected = "\nPlayer A where do you want to place a Destroyer?\n\nCurrent ocean:\n"+expectedHeader+expectedBody+expectedHeader+"\n";
         
      V1ShipFactory shipFactory = new V1ShipFactory();   
      // player.doOnePlacement("Destroyer", (p)->shipFactory.makeDestroyer(p));
      player.doOnePlacement("Destroyer", (p)->shipFactory.makeDestroyer(p));
      System.out.print(bytes.toString());

      assertEquals(expected,bytes.toString());
    }

    // @Test
    // void test_do_one_placement_phase() throws IOException {
    //     ByteArrayOutputStream bytes = new ByteArrayOutputStream();
    //     TextPlayer player = createTextPlayer(4, 4, "B1V\n", bytes);

    //     String prompt =
    //         "Player "+"A"+":"+" you are going to place the following ships (which are all rectangular). "
    //         +"For each ship, type the coordinate of the upper left side of the ship, followed by either H ("
    //         +"for horizontal) or V (for vertical).  For example M4H would place a ship horizontally starting"
    //         +" at M4 and going to the right.  You have\n\n"+"2 \"Submarines\" ships that are 1x2\n"+"3 \"Destroyers\" "
    //         +"that are 1x3\n"+"3 \"Battleships\" that are 1x4\n"+"2 \"Carriers\" that are 1x6\n";

    //     String expectedHeader = "  0|1|2|3\n";
    //     String expectedEmptyBody =
    //         "A  | | |  A\n"+
    //         "B  | | |  B\n"+
    //         "C  | | |  C\n"+
    //         "D  | | |  D\n";
    //     String expectedBody =
    //         "A  | | |  A\n"+
    //         "B  |d| |  B\n"+
    //         "C  |d| |  C\n"+
    //         "D  |d| |  D\n";
   
    //     String expected = expectedHeader + expectedEmptyBody + expectedHeader + prompt 
    //             +"Player A where would you like to put your ship?\n"+ expectedHeader + expectedBody
    //             + expectedHeader;
   
    //     player.doPlacementPhase();
    //     assertEquals(expected, bytes.toString());
    // }
  
}
