package edu.duke.zl328.battleship;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import org.junit.jupiter.api.Test;

public class V1ShipFactoryTest {
    private void checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs) {
        assertEquals(expectedName, testShip.getName());
        for (Coordinate coordinate : expectedLocs) {
            assertEquals(true, testShip.occupiesCoordinates(coordinate));
            assertEquals(expectedLetter, testShip.getDisplayInfoAt(coordinate, true));       
        }
        
    }

    @Test
    public void test_v1ship_factory() {
        Placement v1_2 = new Placement(new Coordinate(1, 2), 'v');
        Placement h1_2 = new Placement(new Coordinate(1, 2), 'h');
        AbstractShipFactory<Character> shipFactory = new V2ShipFactory();
        Ship<Character> dstv = shipFactory.makeDestroyer(v1_2);
        // Ship<Character> carv = shipFactory.makeCarrier(v1_2);
        // Ship<Character> btsv = shipFactory.makeBattleship(v1_2);
        Ship<Character> sbmv = shipFactory.makeSubmarine(v1_2);
        Ship<Character> dsth = shipFactory.makeDestroyer(h1_2);
        // Ship<Character> carh = shipFactory.makeCarrier(h1_2);
        // Ship<Character> btsh = shipFactory.makeBattleship(h1_2);
        Ship<Character> sbmh = shipFactory.makeSubmarine(h1_2);
        checkShip(dstv, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2));
        checkShip(dsth, "Destroyer", 'd', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4));
        // checkShip(carv, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2), new Coordinate(5, 2), new Coordinate(6, 2));
        // checkShip(carh, "Carrier", 'c', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4), new Coordinate(1, 5), new Coordinate(1, 6), new Coordinate(1, 7));
        // checkShip(btsv, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(4, 2));
        // checkShip(btsh, "Battleship", 'b', new Coordinate(1, 2), new Coordinate(1, 3), new Coordinate(1, 4), new Coordinate(1, 5));
        checkShip(sbmv, "Submarine", 's', new Coordinate(1, 2), new Coordinate(2, 2));
        checkShip(sbmh, "Submarine", 's', new Coordinate(1, 2), new Coordinate(1, 3));
    }

    private void invalid_checkShip(Ship<Character> testShip, String expectedName, char expectedLetter, Coordinate... expectedLocs){
        if(testShip.getName()!=expectedName){
          throw new IllegalArgumentException("The name is wrong!");
        }
     
         for(Coordinate e : expectedLocs){
           if( testShip.occupiesCoordinates(e) ==  false){
               throw new IllegalArgumentException("The loc is not in ship!");
           }
          
            if(testShip.getDisplayInfoAt(e, true)!=expectedLetter){
              throw new IllegalArgumentException("The expectedLoc with wrong info!");
            }
         
         }
      }
    

    @Test
    public void invalid_test_v1_ship_factory() {
    V1ShipFactory f = new V1ShipFactory();
    Placement v1_2 = new Placement(new Coordinate(1, 2), 'V');
    Ship<Character> dst = f.makeDestroyer(v1_2);
    assertThrows(IllegalArgumentException.class, () -> invalid_checkShip(dst, "Destroye", 'd', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2)));
    assertThrows(IllegalArgumentException.class, () -> invalid_checkShip(dst, "Destroyer", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2)));

    assertThrows(IllegalArgumentException.class, () -> invalid_checkShip(dst, "Destroyer", 'd', new Coordinate(1, 2),
    new Coordinate(2, 2), new Coordinate(3, 2), new Coordinate(5, 5)));


    Ship<Character> sub = f.makeSubmarine(v1_2);
    assertThrows(IllegalArgumentException.class, () -> invalid_checkShip(sub, "Submarine", 'd', new Coordinate(1, 2), new Coordinate(2, 2)) ); 

    Ship<Character> bat = f.makeBattleship(v1_2);
    assertThrows(IllegalArgumentException.class, () -> invalid_checkShip(bat, "BattleShip", 'b', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2)));

    Ship<Character> car = f.makeCarrier(v1_2);
    assertThrows(IllegalArgumentException.class, () -> invalid_checkShip(car, "Carriera", 'c', new Coordinate(1, 2), new Coordinate(2, 2), new Coordinate(3, 2)));

    Placement h1_2 = new Placement(new Coordinate(1, 2), 'H');
    Ship<Character> sub1 = f.makeSubmarine(h1_2);
    assertThrows(IllegalArgumentException.class, () -> invalid_checkShip(sub1, "Submarine", 'd', new Coordinate(1, 2), new Coordinate(1, 3)) ); 

    }
}
